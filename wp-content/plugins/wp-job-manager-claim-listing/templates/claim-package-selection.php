<?php
/**
 * Claim package selection.
 *
 * @since 3.11.0
 *
 * @package Claim Listing
 * @category Template
 * @author Astoundify
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>

<form id="<?php echo esc_attr( $form->get_form_name() ); ?>" class="job-manager-form wpjmcl_form wpjmcl_form_claim_package" method="post">

	<div class="job_listing_packages_title">

		<?php if ( $packages ) { ?>
			<input type="submit" value="<?php echo esc_attr( $form->get_step_submit() ); ?>" class="button" name="submit">
			<input type="hidden" name="claim_id" value="<?php echo esc_attr( $form->claim_id ); ?>" />
			<input type="hidden" name="step" value="<?php echo esc_attr( $form->get_step() ); ?>">
		<?php } ?>

		<h2><?php _e( 'Choose a package', 'wp-job-manager-claim-listing' ); ?></h2>
	</div><!-- .job_listing_packages_title -->

	<div class="job_listing_packages">

		<?php if ( $packages ) {
			$checked = 1;
			?>

			<ul class="job_packages">

				<li class="package-section"><?php _e( 'Purchase Package:', 'wp-job-manager-claim-listing' ); ?></li>

				<?php foreach ( $packages as $key => $package ) {
					$product = wc_get_product( $package );
					/* Skip if not purchase-able. */
					if ( ! $product->is_type( array( 'job_package', 'job_package_subscription' ) ) || ! $product->is_purchasable() ) {
						continue;
					}
					?>

					<li class="job-package">

						<input type="radio" <?php checked( $checked, 1 );
						$checked = 0; ?> name="job_package" value="<?php echo $product->get_id(); ?>" id="package-<?php echo $product->get_id(); ?>" />

						<label for="package-<?php echo $product->get_id(); ?>"><?php echo $product->get_title(); ?></label><br/>

						<?php echo ( ! empty( $product->get_short_description() ) ) ? apply_filters( 'woocommerce_short_description', $product->get_short_description() ) : '' ?>

						<?php echo $product->get_price_html() . ' '; ?>
						<?php echo $product->get_duration() ? sprintf( _n( '(Listed for %s day)', '(Listed for %s days)', $product->get_duration(), 'wp-job-manager-claim-listing' ), $product->get_duration() ) : ''; ?>

					</li>

				<?php } // End foreach().
?>

			</ul><!-- .job_packages-->

		<?php } else { // package not available  ?>

			<p><?php _e( 'No packages found', 'wp-job-manager-claim-listing' ); ?></p>

		<?php } // End if().
?>

	</div><!-- .job_listing_packages -->

</form>
