<?php
/*
Plugin Name:    Log cleaner for iThemes Security
Plugin URI:     https://wordpress.org/plugins/log-cleaner-for-ithemes-security/
Description:    Delete iThemes Security logs from the database.
Version:        1.2
Author:         Michael Ott
Author URI:     https://rocketapps.com.au/
Text Domain:    log-cleaner
License:        GPL2
Domain Path:    /languages/
*/

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

// Look for translation file.
function load_log_cleaner_textdomain() {
    load_plugin_textdomain( 'log-cleaner', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
}
add_action( 'plugins_loaded', 'load_log_cleaner_textdomain' );

// Create admin page under the Tools menu.
if ( is_multisite() ) {
    add_action('network_admin_menu', 'create_network_cleaner_submenu');
    function create_network_cleaner_submenu() {
        $icon =  plugins_url('images/', __FILE__ ) . 'shield.svg';
        add_menu_page( "ITSec Log Cleaner", "ITSec Log Cleaner", 'manage_options', 'log-cleaner', 'generate_page_content', $icon);	
    }
} else {
    add_action('admin_menu', 'create_tools_cleaner_submenu');
    function create_tools_cleaner_submenu() {
        add_management_page( 'ITSec Log Cleaner', 'ITSec Log Cleaner', 'manage_options', 'log-cleaner', 'generate_page_content' );
    }
}

// Add custom CSS to admin
function log_cleaner_admin_style() {
    $plugin_data = get_plugin_data( __FILE__ );
    $plugin_version = $plugin_data['Version'];
	$plugin_directory = plugins_url('css/', __FILE__ );
    wp_enqueue_style('log-cleaner-style-admin', $plugin_directory . 'log-cleaner.css', '', $plugin_version);
}
add_action('admin_enqueue_scripts', 'log_cleaner_admin_style');

// Admin page.
function generate_page_content() { ?>
    
    <div class="wrap">
        <form action="" method="post">
            <?php
                $page = $_GET["page"];

                if (isset($_POST['submit']) && wp_verify_nonce($_POST['things'], 'delete-things')) {

                    if (isset($_POST['logs']) || isset($_POST['dashboard']) || isset($_POST['lockouts']) || isset($_POST['temp'])) {

                        global $wpdb;
                        $charset_collate        = $wpdb->get_charset_collate();
                        $lockouts_table         = $wpdb->prefix . 'itsec_lockouts';
                        $log_table              = $wpdb->prefix . 'itsec_log';
                        $logs_table             = $wpdb->prefix . 'itsec_logs';
                        $dashboard_table        = $wpdb->prefix . 'itsec_dashboard_events';
                        $temp_table             = $wpdb->prefix . 'itsec_temp';

                        if (isset($_POST['lockouts']) || isset($_POST['all'])) {
                            $wpdb->query("TRUNCATE TABLE " . $lockouts_table);
                        }

                        if (isset($_POST['logs']) || isset($_POST['all'])) {
                            $wpdb->query("TRUNCATE TABLE " . $log_table);
                            $wpdb->query("TRUNCATE TABLE " . $logs_table);
                        }

                        if (function_exists('itsec_pro_load_textdomain')) { // if iTheme Sec Pro is activated
                            if (isset($_POST['dashboard']) || isset($_POST['all'])) {
                                $wpdb->query("TRUNCATE TABLE " . $dashboard_table);
                            }
                        }
                        
                        if (isset($_POST['temp']) || isset($_POST['all'])) {
                            $wpdb->query("TRUNCATE TABLE " . $temp_table);
                        }
                        ?>

                        <div id="message" class="updated notice notice-success is-dismissible" style="margin: 20px 0;">
                            <p><?php _e("The selected logs have been deleted.", 'log-cleaner'); ?></p>
                            <button type="button" class="notice-dismiss"><span class="screen-reader-text"><?php _e("Dismiss this notice.", 'log-cleaner'); ?></span></button>
                        </div>

                    <?php } else { ?>
                        <div id="message" class="error notice notice-success is-dismissible" style="margin: 20px 0;">
                            <p><?php _e("You need to select at least one item to delete.", 'log-cleaner'); ?></p>
                            <button type="button" class="notice-dismiss"><span class="screen-reader-text"><?php _e("Dismiss this notice.", 'log-cleaner'); ?></span></button>
                        </div>
                    <?php  }

                }
            ?>

            <h1><?php _e('Log cleaner for iThemes Security', 'log-cleaner'); ?></h1>

            <?php 
                global $wpdb;
                $itsec_lockouts = $wpdb->prefix . 'itsec_lockouts';
                $lockouts_query = "SELECT count(*) from $itsec_lockouts";
                $num_lockouts   = $wpdb->get_var($lockouts_query);

                /* itsec_log table */
                $itsec_log  = $wpdb->prefix . 'itsec_log';
                $log_query  = "SELECT count(*) from $itsec_log";
                $num_log    = $wpdb->get_var($log_query);

                /* itsec_logs table */
                $itsec_logs = $wpdb->prefix . 'itsec_logs';
                $logs_query = "SELECT count(*) FROM $itsec_logs";
                $num_logs   = $wpdb->get_var($logs_query);

                if (function_exists('itsec_pro_load_textdomain')) { // if iTheme Sec Pro is activated
                    /* itsec_dashboard_events table */
                    $dashboard_events       = $wpdb->prefix . 'itsec_dashboard_events';
                    $dashboard_events_query = "SELECT count(*) FROM $dashboard_events";
                    $num_dashboard_events   = $wpdb->get_var($dashboard_events_query);
                }

                /* itsec_temp table */
                $itsec_temp = $wpdb->prefix . 'itsec_temp';
                $temp_query = "SELECT count(*) from $itsec_temp";
                $num_temps  = $wpdb->get_var($temp_query);
 
                $combined_logs  = $num_log + $num_logs;                
                $total          = $num_lockouts + $num_temps + $num_dashboard_events + $combined_logs;

                global $current_user;

                if ( is_multisite() ) {
                    $network = 'network';
                } else {
                    $network = '';
                }
                $log_link = $network . 'admin.php?page=itsec-logs';
            ?>

            <?php if($total > 0) { // If there are logs ?>
            <p><?php printf( __( "Note: Continuing here will delete the selected iThemes Security logs from the database. You absolutely can not undo this action. If in doubt, <a href='%s'>view the logs first</a> or backup your database.", "log-cleaner" ), $log_link ); ?></p>
            <?php } ?>

            <p class="delete-logs-notice"><?php printf( __( "<a href='%s'>View logs</a>", "log-cleaner" ), $log_link ); ?></p>

            <?php if($total > 0) { // If there are logs ?>

            <div class="log-cleaner boxy">

                <p><strong><?php _e("Clear the following log tables: ", 'log-cleaner'); ?></strong></p>

                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tbody>
                        <tr class="all-items">
                            <td><input type="checkbox" name="all" id="all" /></td>
                            <td><?php _e("All", 'log-cleaner'); ?></td>
                            <td></td>
                        </tr>
                        <?php if($combined_logs > 0) { ?>
                        <tr>
                            <td><input type="checkbox" name="logs" class="other-items" /></td>
                            <td><?php _e("Security logs", 'log-cleaner'); ?></td>
                            <td><?php echo $combined_logs; ?> <?php _e("rows", 'log-cleaner'); ?></td>
                        </tr>
                        <?php } ?>
                        <?php if (function_exists('itsec_pro_load_textdomain')) { // if iTheme Sec Pro is activated
                            if($num_dashboard_events > 0) { ?>
                        <tr>
                            <td><input type="checkbox" name="dashboard" class="other-items" /></td>
                            <td><?php _e("Dashboard logs", 'log-cleaner'); ?></td>
                            <td><?php echo $num_dashboard_events; ?> <?php _e("rows", 'log-cleaner'); ?></td>
                        </tr>
                        <?php }
                        } ?>
                        <?php if($num_lockouts > 0) { ?>
                        <tr>
                            <td><input type="checkbox" name="lockouts" class="other-items" /></td>
                            <td><?php _e("Lockouts", 'log-cleaner'); ?></td>
                            <td><?php echo $num_lockouts; ?> <?php _e("rows", 'log-cleaner'); ?></td>
                        </tr>
                        <?php } ?>
                        <?php if($num_temps > 0) { ?>
                        <tr>
                            <td><input type="checkbox" name="temp" class="other-items" /></td>
                            <td><?php _e("Temps", 'log-cleaner'); ?></td>
                            <td><?php echo $num_temps; ?> <?php _e("rows", 'log-cleaner'); ?></td>
                        </tr>
                        <?php } ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr class="total">
                            <td></td>
                            <td>Total:</td>
                            <td><?php echo $total; ?> <?php _e("rows", 'log-cleaner'); ?></td>
                        </tr>
                    </tfoot>
                </table>
                
                <script>
                    jQuery('#all').click(function(event) {   
                        if(this.checked) {
                            // Iterate each checkbox
                            jQuery(':checkbox').each(function() {
                                this.checked = true;
                                jQuery( event.target ).closest( 'tbody tr' ).addClass( 'active' );
                                jQuery('tbody tr').addClass('active');                   
                            });
                        } else {
                            jQuery(':checkbox').each(function() {
                                this.checked = false;    
                                jQuery( event.target ).closest( 'tbody tr' ).removeClass( 'active' );
                                jQuery('tbody tr').removeClass('active');                       
                            });
                        }
                    });
                    jQuery('.other-items').click(function(event) {
                        jQuery( event.target ).closest( 'tbody tr' ).toggleClass( 'active' );
                        jQuery('#all').prop("checked", false);
                        jQuery('.all-items').removeClass('active');  
                    });
                </script>
            </div>

            <?php  // If the total number of log entries is not 0, and if you're an administrator
                if(current_user_can( 'manage_options' )) { ?>
                <input type="submit" name="submit" class="button button-primary" value="<?php _e('Clear logs', 'log-cleaner'); ?>" onclick="return confirm('<?php _e('This is your last chance. Are you sure?', 'log-cleaner'); ?>')" />
                <?php wp_nonce_field( 'delete-things','things' ) ?>
            <?php } ?>

            <?php } else { // Otherwise, all logs are clear ?>
                <p class="all-clear">&#10004; <?php _e("All logs are clear.", 'log-cleaner'); ?></p>
            <?php } ?>
    
        </form>
    </div>

    <?php require_once('my-tools.php'); ?>

<?php }
