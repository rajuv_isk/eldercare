<?php
/*
Plugin Name: Featured Member Blog Posts
Plugin URI: 
Description: This plugin adds a Featured Members blog widget.
Version: 1.0
Author: ISK
Author URI: 
License: GPL2
*/

// The widget class
class Featured_Member_Blog_Post_Widget extends WP_Widget {

	// Main constructor
	public function __construct() {
		parent::__construct(
			'my_featured_member_blog_post_widget',
			__( 'Featured Member Blog Posts', 'listify' ),
			array(
				'customize_selective_refresh' => true,
			)
		);
	}

	// The widget form (for the backend )
	public function form( $instance ) {

		// Set widget defaults
		$defaults = array(
			'title'    => '',
			'text'     => '',
			'textarea' => '',
			'checkbox' => '',
			'select'   => '',
		);
		
		// Parse current settings with defaults
		extract( wp_parse_args( ( array ) $instance, $defaults ) ); ?>

		<?php // Widget Title ?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php _e( 'Widget Title', 'listify' ); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>

		<?php // Number Field ?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'text' ) ); ?>"><?php _e( 'Number to show:', 'listify' ); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'text' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'text' ) ); ?>" type="number" step="3" min="0" max="30" value="<?php echo esc_attr( $text ); ?>" />
		</p>		

	<?php }

	// Update widget settings
	public function update( $new_instance, $old_instance ) { 
		$instance = $old_instance;
		$instance['title']    = isset( $new_instance['title'] ) ? wp_strip_all_tags( $new_instance['title'] ) : '';
		$instance['text']     = isset( $new_instance['text'] ) ? wp_strip_all_tags( $new_instance['text'] ) : '';
		$instance['textarea'] = isset( $new_instance['textarea'] ) ? wp_kses_post( $new_instance['textarea'] ) : '';
		$instance['checkbox'] = isset( $new_instance['checkbox'] ) ? 1 : false;
		$instance['select']   = isset( $new_instance['select'] ) ? wp_strip_all_tags( $new_instance['select'] ) : '';
		return $instance;
	}

	// Display the widget
	public function widget( $args, $instance ) {

		extract( $args );

		// Check the widget options
		$title    = isset( $instance['title'] ) ? apply_filters( 'widget_title', $instance['title'] ) : '';
		$number_toshow     = isset( $instance['text'] ) ? $instance['text'] : '';

		

		// Display the widget
		$job_id = get_the_ID();
		$job_author_id = get_post_field( 'post_author', $job_id );
		$job_featured = get_post_field( '_featured', $job_id );

		if ($job_featured == 1) {
			

		$args = array(
			'post_type' => 'post',
			'post_status' => 'publish',
			'posts_per_page' => $number_toshow,
			'orderby' => 'rand',
			'author'        => $job_author_id
			
		);
		$result='';
        // query
		$myposts = new WP_Query( $args );;
		$num_of_jobs = $myposts->post_count;
		if($num_of_jobs>0)
		{
			$count = 1;

			//echo '<div postid="'.$jobmetas.'" class="widget-text wp_widget_plugin_box">';
		// WordPress core before_widget hook (always include )
		echo $before_widget;

			// Display widget title if defined
		if ( $title ) {
			echo $before_title . $title . $after_title;
		}
        // begin loop
        echo '<ul class="">';
        while($myposts->have_posts()) : $myposts->the_post();
        	//get_template_part( 'content', 'recent-posts' );
        	?>
        	<li id="post-<?php the_ID(); ?>">
        		<a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a>
        	</li>


        	<!-- <li id="post-<?php the_ID(); ?>" class="job_listing type-job_listing card-style--default style-grid listing-card type-job_listing style-grid">
        		<div class="content-box">
        		<div <?php echo apply_filters( 'listify_cover', 'entry-header entry-cover my-featured-blog entry-cover--grid-cover' ); ?>>
        			<div class="cover-wrapper cover-wrapper--entry-grid">
        				<div class="entry-meta entry-meta--grid">
        					<span class="entry-category">
        						<?php echo get_the_date(); ?>
        					</span>
        				</div>

        				<h3 class="job_listing-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h3>

        				<?php if ( $excerpt ) : ?>
        					<div class="entry-summary">
        						<?php the_excerpt(); ?>
        					</div>
        				<?php endif; ?>

        				<footer class="entry-footer">
        					<a href="<?php the_permalink(); ?>" class="entry-read-more"><?php _e( 'Read More', 'listify' ); ?></a>
        				</footer>
        			</div>
        		</div>
        	</div>
        	</li> --><!-- #post-## -->

        	<?php
			// rinse and repeat
            $count++;
        endwhile;
        wp_reset_postdata();

        // WordPress core after_widget hook (always include )
		echo $after_widget;

		 echo '</ul>';
			
		}



		//echo '</div>';

		
		}

		

	}

}

// Register the widget
function add_featured_member_blog_post_widget() {
	register_widget( 'Featured_Member_Blog_Post_Widget' );
}
add_action( 'widgets_init', 'add_featured_member_blog_post_widget' );