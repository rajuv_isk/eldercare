<?php

/*
Plugin Name: WP All Import - Listify Add-On
Plugin URI: http://www.wpallimport.com/
Description: Supporting imports into the Listify theme.
Version: 1.1.1
Author: Soflyy
*/


include "rapid-addon.php";

$listify_addon = new RapidAddon( 'Listify Add-On', 'listify_addon' );

$listify_addon->disable_default_images();

$listify_addon->import_images( 'listify_addon_listing_gallery', 'Listing Gallery' );

function listify_addon_listing_gallery( $post_id, $attachment_id, $image_filepath, $import_options ) {

    // build gallery_images
    $new_url = wp_get_attachment_url( $attachment_id );

    $urls = get_post_meta( $post_id, '_gallery_images', true );

    $new_urls = array();

    if ( ! empty( $urls ) ) {
        foreach( $urls as $key => $url ) {

            $new_urls[] = $url;

        }
    }

    $new_urls[] = $new_url;

    update_post_meta( $post_id, '_gallery_images', $new_urls );

    //build gallery
    $new_id = $attachment_id;

    $ids = get_post_meta( $post_id, '_gallery', true );

    $new_ids = array();

    if ( ! empty( $ids ) ) {
        foreach( $ids as $key => $id ) {

            $new_ids[] = $id;

        }
    }

    $new_ids[] = $new_id;

    update_post_meta( $post_id, '_gallery', $new_ids );

}

$listify_addon->add_field(
	'_job_location',
	'Location',
	'radio', 
	array(
		'search_by_address' => array(
			'Search by Address',
			$listify_addon->add_options( 
				$listify_addon->add_field(
					'job_address',
					'Job Address',
					'text'
				),
				'Google Geocode API Settings', 
				array(
					$listify_addon->add_field(
						'address_geocode',
						'Request Method',
						'radio',
						array(
							'address_no_key' => array(
								'No API Key',
								'Limited number of requests.'
							),
							'address_google_developers' => array(
								'Google Maps Standard API Key - <a href="https://developers.google.com/maps/documentation/geocoding/get-api-key#key">Get free API key</a>',
								$listify_addon->add_field(
									'address_google_developers_api_key', 
									'API Key', 
									'text'
								),
								'Up to 2500 requests per day and 5 requests per second.'
							),
							'address_google_for_work' => array(
								'Google Maps Premium Client ID & Digital Signature - <a href="https://developers.google.com/maps/premium/">Sign up for Google Maps Premium Plan</a>',
								$listify_addon->add_field(
									'address_google_for_work_client_id', 
									'Google Maps Premium Client ID', 
									'text'
								), 
								$listify_addon->add_field(
									'address_google_for_work_digital_signature', 
									'Google Maps Premium Digital Signature', 
									'text'
								),
								'Up to 100,000 requests per day and 10 requests per second'
							)
						) // end Request Method options array
					), // end Request Method nested radio field 

				) // end Google Geocode API Settings fields
			) // end Google Gecode API Settings options panel
		), // end Search by Address radio field
		'search_by_coordinates' => array(
			'Search by Coordinates',
			$listify_addon->add_field(
				'job_lat', 
				'Latitude', 
				'text', 
				null, 
				'Example: 34.0194543'
			),
			$listify_addon->add_options( 
				$listify_addon->add_field(
					'job_lng', 
					'Longitude', 
					'text', 
					null, 
					'Example: -118.4911912'
				), 
				'Google Geocode API Settings', 
				array(
					$listify_addon->add_field(
						'coord_geocode',
						'Request Method',
						'radio',
						array(
							'coord_no_key' => array(
								'No API Key',
								'Limited number of requests.'
							),
							'coord_google_developers' => array(
								'Google Maps Standard API Key - <a href="https://developers.google.com/maps/documentation/geocoding/get-api-key#key">Get free API key</a>',
								$listify_addon->add_field(
									'coord_google_developers_api_key', 
									'API Key', 
									'text'
								),
								'Up to 2500 requests per day and 5 requests per second.'
							),
							'coord_google_for_work' => array(
								'Google Maps Premium Client ID & Digital Signature - <a href="https://developers.google.com/maps/premium/">Sign up for Google Maps Premium Plan</a>',
								$listify_addon->add_field(
									'coord_google_for_work_client_id', 
									'Google Maps Premium Client ID', 
									'text'
								), 
								$listify_addon->add_field(
									'coord_google_for_work_digital_signature', 
									'Google Maps Premium Digital Signature', 
									'text'
								),
								'Up to 100,000 requests per day and 10 requests per second'
							)
						) // end Geocode API options array
					), // end Geocode nested radio field 
					
				) // end Geocode settings
			) // end coordinates Option panel
		) // end Search by Coordinates radio field
	) // end Job Location radio field
);

$listify_addon->add_field( '_application', 'Contact email/URL', 'text', null, 'This field is required for the "application" area to appear beneath the listing.');

$listify_addon->add_field( '_company_website', 'Company Website', 'text' );

$listify_addon->add_field( '_company_avatar', 'Company Logo/Avatar', 'image' );

$social_setting = get_option( 'theme_mods_listify' );

$social_setting_child = get_option( 'theme_mods_listify-child' );

if ( ( is_array( $social_setting ) and array_key_exists( 'social-association', $social_setting ) and $social_setting['social-association'] == 'listing' ) or ( is_array( $social_setting_child ) and array_key_exists( 'social-association', $social_setting_child ) and $social_setting_child['social-association'] == 'listing' ) ) {

    // If the user has decided to associate the social profile fields with listings
    // We'll add them as fields in the import here.

    $listify_addon->add_options(
        null,
        'Social Profile Fields',
        array(
                $listify_addon->add_field( '_company_facebook', 'Facebook URL', 'text' ),

                $listify_addon->add_field( '_company_twitter', 'Twitter URL', 'text' ),

                $listify_addon->add_field( '_company_googleplus', 'Google+ URL', 'text' ),

                $listify_addon->add_field( '_company_linkedin', 'LinkedIn URL', 'text' ),

                $listify_addon->add_field( '_company_instagram', 'Instagram URL', 'text' ),

                $listify_addon->add_field( '_company_github', 'GitHub URL', 'text' ),

                $listify_addon->add_field( '_company_pinterest', 'Pinterest URL', 'text' )
            )
        );

}

// field is _company_video, will 'image' add_field support videos?
$listify_addon->add_field( 'video_type', 'Company Video', 'radio',
    array(
        'external' => array(
            'Externally Hosted',
            $listify_addon->add_field( '_company_video_url', 'Video URL', 'text')
        ),
        'local' => array(
            'Locally Hosted',
            $listify_addon->add_field( '_company_video_id', 'Upload Video', 'file')
)));

$listify_addon->add_field( '_job_expires', 'Listing Expiry Date', 'text', null, 'Import date in any strtotime compatible format.');

$listify_addon->add_field( '_phone', 'Company Phone', 'text' );

$listify_addon->add_field( '_claimed', 'Claimed', 'radio', 
    array(
        '0' => 'No',
        '1' => 'Yes'
    ),
    'The owner has been verified.'
);

$listify_addon->add_field( '_featured', 'Featured Listing', 'radio', 
    array(
        '0' => 'No',
        '1' => 'Yes'
    ),
    'Featured listings will be sticky during searches, and can be styled differently.'
);

$listify_addon_listify_version = get_option( 'listify_version' );

$listify_addon_listify_version = str_replace( ".", "", $listify_addon_listify_version );

if ( $listify_addon_listify_version < "200" ) {
    
    $listify_addon->add_title( 'Hours of Operation', 'Use Closed, 24h, or a time (for example: 8:30 am).' );

} else {

    $listify_addon->add_title( 'Hours of Operation', 'Use Closed, 24h, or a time (for example: 8:30 am). Seperate multiple open close times with a comma, for example: 8:30 am, 12:00 pm.' );

}

$listify_addon->add_text( '<br><b>Monday</b>' );

$listify_addon->add_field( 'monday_open', 'Open', 'text' );

$listify_addon->add_field( 'monday_close', 'Close', 'text' );

$listify_addon->add_text( '<br><br><b>Tuesday</b>' );

$listify_addon->add_field( 'tuesday_open', 'Open', 'text' );

$listify_addon->add_field( 'tuesday_close', 'Close', 'text' );

$listify_addon->add_text( '<br><br><b>Wednesday</b>' );

$listify_addon->add_field( 'wednesday_open', 'Open', 'text' );

$listify_addon->add_field( 'wednesday_close', 'Close', 'text' );

$listify_addon->add_text( '<br><br><b>Thursday</b>' );

$listify_addon->add_field( 'thursday_open', 'Open', 'text' );

$listify_addon->add_field( 'thursday_close', 'Close', 'text' );

$listify_addon->add_text( '<br><br><b>Friday</b>' );

$listify_addon->add_field( 'friday_open', 'Open', 'text' );

$listify_addon->add_field( 'friday_close', 'Close', 'text' );

$listify_addon->add_text( '<br><br><b>Saturday</b>' );

$listify_addon->add_field( 'saturday_open', 'Open', 'text' );

$listify_addon->add_field( 'saturday_close', 'Close', 'text' );

$listify_addon->add_text( '<br><br><b>Sunday</b>' );

$listify_addon->add_field( 'sunday_open', 'Open', 'text' );

$listify_addon->add_field( 'sunday_close', 'Close', 'text' );

if ( $listify_addon_listify_version >= "200" ) {

	$listify_addon->add_field( '_job_hours_timezone', 'Timezone', 'text', null, 'Enter a valid timezone. Example values: "America/Los Angeles", "America/Indiana/Indianapolis", "UTC+5", "UTC+5:30" (without quotes). Leave blank to use UTC+0.' );

}

// Check whether the user wants to add the featured image to the gallery as well.
$listify_addon->add_options(
    null,
    'Advanced Options',
    array(
        $listify_addon->add_field(
            'add_featured_img_to_gallery', 
            'Add featured image to gallery?',
            'radio',
            array(
                '0' => "No",
                '1' => "Yes"
                )
            )
        )
    );

$listify_addon->set_import_function( 'listify_addon_import' );

$listify_addon->admin_notice(
    'The Listify Add-On requires WP All Import <a href="http://www.wpallimport.com/order-now/?utm_source=free-plugin&utm_medium=dot-org&utm_campaign=listify" target="_blank">Pro</a> or <a href="http://wordpress.org/plugins/wp-all-import" target="_blank">Free</a>, and the <a href="https://astoundify.com/go/wp-all-import-buy-listify/">Listify</a> theme.',
    array( 
        'themes' => array( 'Listify' )
) );

$listify_addon->run( array(
        'themes' => array( 'Listify' ),
        'post_types' => array( 'job_listing' ) 
) );

function listify_addon_import( $post_id, $data, $import_options, $article ) {
    
    global $listify_addon;
    
    // all fields except for location, slider and image fields
    $fields = array(
        '_application',
        '_company_website',
        '_phone',
        '_claimed',
        '_featured',
        'add_featured_img_to_gallery'
    );

    $social_setting = get_option( 'theme_mods_listify' );

    $social_setting_child = get_option( 'theme_mods_listify-child' );

   if ( ( array_key_exists('social-association',$social_setting) and $social_setting['social-association'] == 'listing' ) or is_array($social_setting_child) and array_key_exists('social-association',$social_setting_child) and $social_setting_child['social-association'] == 'listing') {
        // If they're associating social profile fields with the listings.
        // Add the fields.

        array_push( $fields, '_company_facebook', '_company_twitter', '_company_googleplus', '_company_linkedin', '_company_instagram', '_company_github', '_company_pinterest' );

    }

    // update everything in fields arrays
    foreach ( $fields as $field ) {

        if ( empty( $article['ID'] ) or $listify_addon->can_update_meta( $field, $import_options ) ) {

            update_post_meta( $post_id, $field, $data[$field] );

        }
    }

    // update listing expiration date
    $field = '_job_expires';

    $date = $data[$field];

    $date = strtotime( $date );

    if ( empty( $article['ID'] ) or $listify_addon->can_update_meta( $field, $import_options ) ) {

        if ( !empty( $date ) ) {

            $date = date( 'Y-m-d', $date );

            update_post_meta( $post_id, $field, $date );

        }

    }

    // clear image fields to override import settings
    $fields = array(
        'gallery_images',
        'gallery',
        '_company_avatar'
    );

    if ( empty( $article['ID'] ) or $listify_addon->can_update_image( $import_options ) ) {

        foreach ($fields as $field) {

            delete_post_meta($post_id, $field);

        }

    }

    // update company avatar/logo
    if ( empty( $article['ID'] ) or $listify_addon->can_update_meta( '_company_avatar', $import_options ) ) {

        if ( !empty( $data['_company_avatar'] ) ) {

            $avatar_url = wp_get_attachment_url( $data['_company_avatar']['attachment_id'] );

            update_post_meta( $post_id, '_company_avatar', $avatar_url );

        }

    }

    // update video

    if ( empty( $article['ID'] ) or $listify_addon->can_update_meta( '_company_video', $import_options ) ) {

        if ( $data['video_type'] == 'external' ) {

            update_post_meta( $post_id, '_company_video', $data['_company_video_url'] );

        } elseif ( $data['video_type'] == 'local' ) {

            $attachment_id = $data['_company_video_id']['attachment_id'];

            $url = wp_get_attachment_url( $attachment_id );

            update_post_meta( $post_id, '_company_video', $url );
        }
    }

    // update hours
    $field = '_job_hours';

    if ( empty( $article['ID'] ) or $listify_addon->can_update_meta( $field, $import_options ) ) {

        $listify_addon_listify_version = get_option( 'listify_version' );

        $listify_addon_listify_version = str_replace( ".", "", $listify_addon_listify_version );
        
       if ( $listify_addon_listify_version < "200" ) {
            // Use the old array style for the older versions that didn't allow multiple hours.

            $hours = array(
                1 => array(
                    'start' => $data['monday_open'],
                    'end' => $data['monday_close']
                ),
                2 => array(
                    'start' => $data['tuesday_open'],
                    'end' => $data['tuesday_close']
                ),
                3 => array(
                    'start' => $data['wednesday_open'],
                    'end' => $data['wednesday_close']
                ),
                4 => array(
                    'start' => $data['thursday_open'],
                    'end' => $data['thursday_close']
                ),
                5 => array(
                    'start' => $data['friday_open'],
                    'end' => $data['friday_close']
                ),
                6 => array(
                    'start' => $data['saturday_open'],
                    'end' => $data['saturday_close']
                ),
                0 => array(
                    'start' => $data['sunday_open'],
                    'end' => $data['sunday_close']
            ) );

            foreach( $hours as $day => $key ) {

            foreach( $key as $subkey => $value ) {

                if ( strtotime( $value ) != false ) {
					
					$new_value = $value;
						
                } else {

                    $new_value = ucwords( $value );

                }

                $hours[$day][$subkey] = $new_value;

                }
            }

            update_post_meta( $post_id, $field, $hours );
        } else {
            // New use the new array structure that allows multiple business hours.

            $hours = array(
                'mon' => array( $data['monday_open'], $data['monday_close' ] ),
                'tue' => array( $data['tuesday_open'], $data['tuesday_close'] ),
                'wed' => array( $data['wednesday_open'], $data['wednesday_close'] ),
                'thu' => array( $data['thursday_open'], $data['thursday_close'] ),
                'fri' => array( $data['friday_open'], $data['friday_close'] ),
                'sat' => array( $data['saturday_open'], $data['saturday_close'] ),
                'sun' => array( $data['sunday_open'], $data['sunday_close'] )
            );
            $i = 0;

            foreach ( $hours as $day => $times ) {

                if ( isset( $times[0] ) ) { $open = $times[0]; } // All open times.
                if ( isset( $times[1] ) ) { $close = $times[1]; } // All close times.
                unset( $hours[ $day ] ); // Reset array keys.

                // Implode with comma in case they put multiple times
                $open = explode( ",", $open ); 
                $close = explode( ",", $close );


                if ( count( $open ) > 1 && count( $close ) > 1 ) {
                    // There are multiple open and close times.
                    $x = 0; // The array key we'll use.
                    
                    foreach ( $open as $key => $time ) {
                        
                        if ( $datetime = strtotime( trim( $time ) ) ) {
                            // They entered a valid date/time
							$hours[ $day ][ $x ]['open'] = trim( $time );

                        } else {
                            // It's probably "24h" or "Closed"
                            $hours[ $day ][ $x ]['open'] = trim( ucwords( $time ) );                            
                        }

                        if ( isset( $close[ $key ] ) ) {

                            if ( $datetime = strtotime( trim( $close[ $key ] ) ) ) {
                                // They entered a valid date/time
								$hours[ $day ][ $x ]['close'] = trim( $close[ $key ] );
                            } else {
                                // It's probably "24h" or "Closed"
                                $hours[ $day ][ $x ]['close'] = trim( ucwords( $close[ $key ] ) );

                            }

                        } else {
                            // Uh oh, they didn't enter the same amount of close times.
                            $hours[ $day ][ $x ]['close'] = null;

                        }

                        $x++; // Next inner array key.

                    }

                } else {

                    // They only entered 1 opening time.
                    if ( $datetime = strtotime( $open[0] ) ) {
						
						$hours[ $day ][ $x ]['open'] = $open[0];

                    } else {

                        $hours[ $day ][ $i ]['open'] = ucwords( $open[0] );

                    }

                    if ( $datetime = strtotime( $close[0] ) ) {

						$hours[ $day ][ $x ]['close'] = $close[0];

                    } else {
                    
                        $hours[ $day ][ $i ]['close'] = ucwords( $close[0] );

                    }

                }

                $i++; // Next array key.

            }

            update_post_meta( $post_id, $field, $hours );

        }

    }

	if ( $listify_addon_listify_version >= "200" ) {

		$field = '_job_hours_timezone';

		if ( empty( $article['ID'] ) or $listify_addon->can_update_meta( $field, $import_options ) ) {

			if ( empty( $data[ $field ] ) ) {
				// They did not enter a timezone, so we'll default to UTC.
				update_post_meta( $post_id, $field, "UTC+0" );
				update_post_meta( $post_id, '_job_hours_gmt', '0' );
				$listify_addon->log( "<strong>Timezone</strong>: No timezone entered, using UTC+0." );

			} else {
				// Replace spaces with underscores so that we can find the timezone.
				$timezone = str_replace( " ", "_", $data[ $field ] );

				$soflyy_check_valid_utc = array( 'UTC-12', 'UTC-11:30', 'UTC-10:30', 'UTC-10', 'UTC-9:30', 'UTC-9', 'UTC-8:30', 'UTC-8', 'UTC-7:30', 'UTC-7', 'UTC-6:30', 'UTC-6', 'UTC-5:30', 'UTC-5', 'UTC-4:30', 'UTC-4', 'UTC-3:30', 'UTC-3', 'UTC-2:30', 'UTC-2', 'UTC-1:30', 'UTC-1', 'UTC-0:30', 'UTC+0', 'UTC+0:30', 'UTC+1', 'UTC+1:30', 'UTC+2', 'UTC+2:30', 'UTC+3', 'UTC+3:30', 'UTC+4', 'UTC+4:30', 'UTC+5', 'UTC+5:30', 'UTC+5:45', 'UTC+6', 'UTC+6:30', 'UTC+7', 'UTC+7:30', 'UTC+8', 'UTC+8:30', 'UTC+8:45', 'UTC+9', 'UTC+9:30', 'UTC+10', 'UTC+10:30', 'UTC+11', 'UTC+11:30', 'UTC+12', 'UTC+12:45', 'UTC+13', 'UTC+13:45', 'UTC+14' );

				if ( stristr( $timezone, "UTC" ) ) {
					// They used UTC in the string, so we'll check against valid UTC offsets.
					$timezone = strtoupper( $timezone );
					if ( in_array( $timezone, $soflyy_check_valid_utc ) ) {

						$offset = str_replace( array( "UTC", "+", ":30", ":45" ), array( "", "", ".5", ".75" ), $timezone ); // sanitize the offset for Listify
						$listify_addon->log( "<strong>Timezone</strong>: Setting timezone to " . $timezone . "." );
						$timezone = str_replace( array( ":30", ":45" ), array( ".5", ".75" ), $timezone ); // Sanitize the UTC value for Listify
					}

				} else {
					// They entered a timezone string instead of a UTC offset.

					if ( in_array( $timezone, timezone_identifiers_list() ) ) {
						// It is a valid timezone, let's get the offset.
						$Date_Time_Zone = new DateTimeZone( $timezone );
						$date = new DateTime(null, $Date_Time_Zone);
						$offset = $Date_Time_Zone->getOffset( $date )/60/60;
						$listify_addon->log( "<strong>Timezone</strong>: Valid timezone found '" . $timezone . "'. Offset: UTC" . $offset );

					} else {
						// It's not a valid timezone.
						$listify_addon->log( "<strong>Timezone</strong>: This is not a valid timezone: " . $timezone . ". Using UTC+0." );
						$timezone = "UTC+0";
						$offset = "0";

					}
				}

				update_post_meta( $post_id, $field, $timezone );
				update_post_meta( $post_id, '_job_hours_gmt', $offset );

			}

		}
	}
	
    // update job location
    $field   = 'job_address';

    $address = $data[$field];
	
    $lat  = $data['job_lat'];

    $long = $data['job_lng'];

    $latitude = null;
    $longitude = null;
    $formatted_address = null;
    $street_number = null;
    $street = null;
    $city = null;
    $state_short = null;
    $state_long = null;
    $zip = null;
    $country_short = null;
    $country_long = null;
    $job_location = null;
    $geo_status = null;
    $api_key = null;
    $geocoding_failed = false;
    
    //  build search query
    if ( $data['_job_location'] == 'search_by_address' ) {

    	$search = ( !empty( $address ) ? 'address=' . rawurlencode( $address ) : null );
		
    } else {

    	$search = ( !empty( $lat ) && !empty( $long ) ? 'latlng=' . rawurlencode( $lat . ',' . $long ) : null );

    }

    // build api key
    if ( $data['_job_location'] == 'search_by_address' ) {
    
    	if ( $data['address_geocode'] == 'address_google_developers' && !empty( $data['address_google_developers_api_key'] ) ) {
        
	        $api_key = '&key=' . $data['address_google_developers_api_key'];
	    
	    } elseif ( $data['address_geocode'] == 'address_google_for_work' && !empty( $data['address_google_for_work_client_id'] ) && !empty( $data['address_google_for_work_signature'] ) ) {
	        
	        $api_key = '&client=' . $data['address_google_for_work_client_id'] . '&signature=' . $data['address_google_for_work_signature'];

	    }

    } else {

    	if ( $data['coord_geocode'] == 'coord_google_developers' && !empty( $data['coord_google_developers_api_key'] ) ) {
        
	        $api_key = '&key=' . $data['coord_google_developers_api_key'];
	    
	    } elseif ( $data['coord_geocode'] == 'coord_google_for_work' && !empty( $data['coord_google_for_work_client_id'] ) && !empty( $data['coord_google_for_work_signature'] ) ) {
	        
	        $api_key = '&client=' . $data['coord_google_for_work_client_id'] . '&signature=' . $data['coord_google_for_work_signature'];

	    }

    }
	
	// Store _job_location value for later use
	
    if ( $data['_job_location'] == 'search_by_address' ) {

    	$job_location = $address;

    } else {

    	$job_location = $lat . ', ' . $long;

    }

    // if all fields are updateable and $search has a value
    if (  empty( $article['ID'] ) or ( $listify_addon->can_update_meta( $field, $import_options ) && $listify_addon->can_update_meta( '_job_location', $import_options ) && !empty ( $search ) ) ) {
        
        // build $request_url for api call
        $request_url = 'https://maps.googleapis.com/maps/api/geocode/json?' . $search . $api_key;
        $curl        = curl_init();

        curl_setopt( $curl, CURLOPT_URL, $request_url );
        curl_setopt( $curl, CURLOPT_RETURNTRANSFER, 1 );

        $listify_addon->log( '- Getting location data from Geocoding API: '.$request_url );

        $json = curl_exec( $curl );

        curl_close( $curl );
        
        // parse api response
        if ( !empty( $json ) ) {

            $details = json_decode( $json, true );

            if ( array_key_exists( 'status', $details ) ) {
				if ( $details['status'] == 'INVALID_REQUEST' || $details['status'] == 'ZERO_RESULTS' || $details['status'] == 'REQUEST_DENIED' ) {
                    $geocoding_failed = true;
                    goto invalidrequest;
                }
			}

            $address_data = array();

			foreach ( $details['results'][0]['address_components'] as $type ) {
				// Went for type_name here to try to make the if statement a bit shorter,
				// and hopefully clearer as well
				$type_name = $type['types'][0];
				
				if ($type_name == "administrative_area_level_1" || $type_name == "administrative_area_level_2" || $type_name == "country") {
					// short_name & long_name must be stored for these three field types, as
					// the short & long names are stored by WP Job Manager
					$address_data[ $type_name . "_short_name" ] = $type['short_name'];
					$address_data[ $type_name . "_long_name" ] = $type['long_name'];
				} else {
					// The rest of the data from Google Maps can be returned in long format,
					// as the other fields only store data in that format
					$address_data[ $type_name ] = $type['long_name'];
				}

			}
			
			// It's a long list, but this is what WP Job Manager stores in the database
			$geo_status = ($details['status'] == "ZERO_RESULTS") ? 0 : 1;
			
			$latitude  = $details['results'][0]['geometry']['location']['lat'];

            $longitude = $details['results'][0]['geometry']['location']['lng'];

        	$formatted_address = $details['results'][0]['formatted_address'];
			
			$street_number = $address_data['street_number'];
			
			$street = $address_data['route'];

        	$city = $address_data['locality'];

        	$country_short = $address_data['country_short_name'];
			
			$country_long = $address_data['country_long_name'];

        	$zip = $address_data['postal_code'];
			
			// Important because the "geolocation_state_short" & "geolocation_state_long" fields
			// can get data from "administrative_area_level_1" or "administrative_area_level_2",
			// depending on the address that's provided
			$state_short = !empty( $address_data['administrative_area_level_1_short_name'] ) ? $address_data['administrative_area_level_1_short_name'] : $address_data['administrative_area_level_2_short_name'];
			
			$state_long = !empty( $address_data['administrative_area_level_1_long_name'] ) ? $address_data['administrative_area_level_1_long_name'] : $address_data['administrative_area_level_2_long_name'];
			
			// Checks for empty location elements
			
        	if ( empty( $zip ) ) {

			    $listify_addon->log( '<b>WARNING:</b> Google Maps has not returned a Postal Code for this job location.' );

        	}

        	if ( empty( $country_short ) && empty( $country_long ) ) {

			    $listify_addon->log( '<b>WARNING:</b> Google Maps has not returned a Country for this job location.' );

        	}
			
        	if ( empty( $state_short ) && empty( $state_long ) ) {

			    $listify_addon->log( '<b>WARNING:</b> Google Maps has not returned a State for this job location.' );

        	}

        	if ( empty( $city ) ) {

			    $listify_addon->log( '<b>WARNING:</b> Google Maps has not returned a City for this job location.' );

        	}

        	if ( empty( $street_number ) ) {

			    $listify_addon->log( '<b>WARNING:</b> Google Maps has not returned a Street Number for this job location.' );

        	}

        	if ( empty( $street ) ) {

			    $listify_addon->log( '<b>WARNING:</b> Google Maps has not returned a Street Name for this job location.' );

        	}

        } else {
			$listify_addon->log( '<b>WARNING:</b> Could not retrieve response data from Google Maps API.' );
		}
        
    }
    
    // List of location fields to update
	$fields = array(
		'geolocation_lat' => $latitude,
		'geolocation_long' => $longitude,
		'geolocation_formatted_address' => $formatted_address,
		'geolocation_street_number' => $street_number,
		'geolocation_street' => $street,
		'geolocation_city' => $city,
		'geolocation_state_short' => $state_short,
		'geolocation_state_long' => $state_long,
		'geolocation_postcode' => $zip,
		'geolocation_country_short' => $country_short,
		'geolocation_country_long' => $country_long,
		'_job_location' => $job_location
	);

    $listify_addon->log( '- Updating location data' );
    
	// Check if "geolocated" field should be created or deleted
	if ($geo_status == "0") {
		delete_post_meta( $post_id, "geolocated" );
	} elseif ($geo_status == "1") {
		update_post_meta( $post_id, "geolocated", $geo_status );
	} else {
		// Do nothing, it's possible that we didn't get a response from the Google Maps API
	}
	
    foreach ( $fields as $key => $value ) {
        
        if ( empty( $article['ID'] ) or $listify_addon->can_update_meta( $key, $import_options ) && !is_null($value) ) {
			// If the field can be updated, and the value isn't NULL, update the field
            update_post_meta( $post_id, $key, $value );
        } elseif ( empty( $article['ID'] ) or $listify_addon->can_update_meta( $key, $import_options ) ) {
			// Else, if the value for the field returns NULL, delete the field
			delete_post_meta( $post_id, $key, $value );
		} else {
			// Else, do nothing
		}
    }

    invalidrequest:

    if ( $geocoding_failed ) {
        delete_post_meta( $post_id, "geolocated" );
        $listify_addon->log( "WARNING Geocoding failed with status: " . $details['status'] );
        if ( array_key_exists( 'error_message', $details ) ) {
			$listify_addon->log( "WARNING Geocoding error message: " . $details['error_message'] );
		}
    }

}

add_action( 'pmxi_saved_post', 'listify_addon_check_featured', 10, 3 );

function listify_addon_check_featured( $post_id, $xml, $is_update ) {
    
    $add_featured = get_post_meta( $post_id, 'add_featured_img_to_gallery', true );

    if ( $add_featured == '0' || strtolower( $add_featured ) == 'no' ) {

        // They've selected "No" in the "Add featured image to gallery?" setting, so we'll remove
        // it from both of the gallery fields.

        $featured_id = get_post_thumbnail_id( $post_id );

        $ids = get_post_meta( $post_id, '_gallery', true );

        if ( !empty( $featured_id ) && is_string( $featured_id ) ) {
            
            $new_ids_without_featured = array_diff( $ids, array( $featured_id ) );

            update_post_meta( $post_id, '_gallery', $new_ids_without_featured );
        }

        $featured_imgs = get_post_meta( $post_id, '_gallery_images', true );

        $featured_url = get_the_post_thumbnail_url( $post_id );

        if ( !empty( $featured_url ) && is_string( $featured_url ) ) {

            $new_urls_without_featured = array_diff( $features_imgs, array( $featured_url ) );

            update_post_meta( $post_id, '_gallery_images', $new_urls_without_featured );

        }

    }

    delete_post_meta( $post_id, 'add_featured_img_to_gallery' );

}

function listify_addon_admin_scripts() {
	$current_screen = get_current_screen();
	
	$listify_addon_listify_version = get_option( 'listify_version' );

	$listify_addon_listify_version = str_replace( ".", "", $listify_addon_listify_version );
	
	// Check that we're on a import page, and that Listify v2.0 or greater is active
	if ( ($current_screen->id == "all-import_page_pmxi-admin-import" || $current_screen->id == "all-import_page_pmxi-admin-manage") && $listify_addon_listify_version >= "200") {
		
		wp_enqueue_script( 'listify-addon-js', plugin_dir_url( __FILE__ ) . '/js/listify-addon-js.js', array( 'jquery' ), '1.0.0', true );
		
	}
}
add_action( 'admin_enqueue_scripts', 'listify_addon_admin_scripts' );

add_action( 'pmxi_before_post_import', 'wpai_listify_ensure_location_data_is_imported', 10, 1 );

function wpai_listify_ensure_location_data_is_imported ($import_id) {

	$import = new PMXI_Import_Record();
	$import_object = $import->getById($import_id);
	$post_type = $import_object->options['custom_type'];
	
	if ($post_type == "job_listing") {
		remove_all_actions('job_manager_job_location_edited');
	}

}