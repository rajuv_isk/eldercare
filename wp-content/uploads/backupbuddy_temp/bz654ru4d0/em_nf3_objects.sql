CREATE TABLE `em_nf3_objects` (  `id` int(11) NOT NULL AUTO_INCREMENT,  `type` longtext CHARACTER SET utf8mb4,  `title` longtext COLLATE utf8mb4_unicode_ci,  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,  `updated_at` datetime DEFAULT NULL,  `object_title` longtext CHARACTER SET utf8mb4,  UNIQUE KEY `id` (`id`)) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40000 ALTER TABLE `em_nf3_objects` DISABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 0;
SET UNIQUE_CHECKS = 0;
/*!40000 ALTER TABLE `em_nf3_objects` ENABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 1;
SET UNIQUE_CHECKS = 1;
