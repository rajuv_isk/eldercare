CREATE TABLE `em_itsec_geolocation_cache` (  `location_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,  `location_host` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,  `location_lat` decimal(10,8) NOT NULL,  `location_long` decimal(11,8) NOT NULL,  `location_label` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,  `location_credit` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,  `location_time` datetime NOT NULL,  PRIMARY KEY (`location_id`),  UNIQUE KEY `location_host` (`location_host`),  KEY `location_time` (`location_time`)) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40000 ALTER TABLE `em_itsec_geolocation_cache` DISABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 0;
SET UNIQUE_CHECKS = 0;
/*!40000 ALTER TABLE `em_itsec_geolocation_cache` ENABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 1;
SET UNIQUE_CHECKS = 1;
