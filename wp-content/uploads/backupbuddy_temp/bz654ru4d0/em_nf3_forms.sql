CREATE TABLE `em_nf3_forms` (  `id` int(11) NOT NULL AUTO_INCREMENT,  `title` longtext COLLATE utf8mb4_unicode_ci,  `key` longtext COLLATE utf8mb4_unicode_ci,  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,  `updated_at` datetime DEFAULT NULL,  `views` int(11) DEFAULT NULL,  `subs` int(11) DEFAULT NULL,  `form_title` longtext CHARACTER SET utf8mb4,  `default_label_pos` varchar(15) CHARACTER SET utf8mb4 DEFAULT NULL,  `show_title` bit(1) DEFAULT NULL,  `clear_complete` bit(1) DEFAULT NULL,  `hide_complete` bit(1) DEFAULT NULL,  `logged_in` bit(1) DEFAULT NULL,  `seq_num` int(11) DEFAULT NULL,  UNIQUE KEY `id` (`id`)) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40000 ALTER TABLE `em_nf3_forms` DISABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 0;
SET UNIQUE_CHECKS = 0;
INSERT INTO `em_nf3_forms` VALUES('1', 'Contact Eldercare Matters', '', '2019-09-20 19:50:04', NULL, NULL, NULL, 'Contact Eldercare Matters', 'above', '0', '1', '1', '0', '381');
INSERT INTO `em_nf3_forms` VALUES('2', 'Contact this Business', '', '2019-05-14 16:52:14', NULL, NULL, NULL, 'Contact this Business', 'above', '0', '1', '1', '0', '1');
/*!40000 ALTER TABLE `em_nf3_forms` ENABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 1;
SET UNIQUE_CHECKS = 1;
