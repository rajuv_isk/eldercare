CREATE TABLE `em_bp_notifications` (  `id` bigint(20) NOT NULL AUTO_INCREMENT,  `user_id` bigint(20) NOT NULL,  `item_id` bigint(20) NOT NULL,  `secondary_item_id` bigint(20) DEFAULT NULL,  `component_name` varchar(75) COLLATE utf8mb4_unicode_ci NOT NULL,  `component_action` varchar(75) COLLATE utf8mb4_unicode_ci NOT NULL,  `date_notified` datetime NOT NULL,  `is_new` tinyint(1) NOT NULL DEFAULT '0',  PRIMARY KEY (`id`),  KEY `item_id` (`item_id`),  KEY `secondary_item_id` (`secondary_item_id`),  KEY `user_id` (`user_id`),  KEY `is_new` (`is_new`),  KEY `component_name` (`component_name`),  KEY `component_action` (`component_action`),  KEY `useritem` (`user_id`,`is_new`)) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40000 ALTER TABLE `em_bp_notifications` DISABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 0;
SET UNIQUE_CHECKS = 0;
/*!40000 ALTER TABLE `em_bp_notifications` ENABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 1;
SET UNIQUE_CHECKS = 1;
