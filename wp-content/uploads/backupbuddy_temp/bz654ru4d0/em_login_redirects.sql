CREATE TABLE `em_login_redirects` (  `rul_type` enum('user','role','level','all','register') COLLATE latin1_general_ci NOT NULL,  `rul_value` varchar(191) COLLATE latin1_general_ci DEFAULT NULL,  `rul_url` longtext COLLATE latin1_general_ci,  `rul_url_logout` longtext COLLATE latin1_general_ci,  `rul_order` int(2) NOT NULL DEFAULT '0',  UNIQUE KEY `rul_type` (`rul_type`,`rul_value`)) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40000 ALTER TABLE `em_login_redirects` DISABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 0;
SET UNIQUE_CHECKS = 0;
INSERT INTO `em_login_redirects` VALUES('all', NULL, NULL, NULL, '0');
INSERT INTO `em_login_redirects` VALUES('register', NULL, NULL, NULL, '0');
/*!40000 ALTER TABLE `em_login_redirects` ENABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 1;
SET UNIQUE_CHECKS = 1;
