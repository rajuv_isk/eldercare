CREATE TABLE `em_statistics_historical` (  `ID` bigint(20) NOT NULL AUTO_INCREMENT,  `category` varchar(25) NOT NULL,  `page_id` bigint(20) NOT NULL,  `uri` varchar(255) NOT NULL,  `value` bigint(20) NOT NULL,  PRIMARY KEY (`ID`),  UNIQUE KEY `page_id` (`page_id`),  UNIQUE KEY `uri` (`uri`),  KEY `category` (`category`)) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40000 ALTER TABLE `em_statistics_historical` DISABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 0;
SET UNIQUE_CHECKS = 0;
/*!40000 ALTER TABLE `em_statistics_historical` ENABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 1;
SET UNIQUE_CHECKS = 1;
