CREATE TABLE `em_statistics_exclusions` (  `ID` int(11) NOT NULL AUTO_INCREMENT,  `date` date NOT NULL,  `reason` varchar(255) DEFAULT NULL,  `count` bigint(20) NOT NULL,  PRIMARY KEY (`ID`),  KEY `date` (`date`),  KEY `reason` (`reason`)) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40000 ALTER TABLE `em_statistics_exclusions` DISABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 0;
SET UNIQUE_CHECKS = 0;
/*!40000 ALTER TABLE `em_statistics_exclusions` ENABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 1;
SET UNIQUE_CHECKS = 1;
