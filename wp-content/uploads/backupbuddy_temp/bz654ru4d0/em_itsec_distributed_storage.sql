CREATE TABLE `em_itsec_distributed_storage` (  `storage_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,  `storage_group` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,  `storage_key` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',  `storage_chunk` int(11) NOT NULL DEFAULT '0',  `storage_data` longtext COLLATE utf8mb4_unicode_ci NOT NULL,  `storage_updated` datetime NOT NULL,  PRIMARY KEY (`storage_id`),  UNIQUE KEY `storage_group__key__chunk` (`storage_group`,`storage_key`,`storage_chunk`)) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40000 ALTER TABLE `em_itsec_distributed_storage` DISABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 0;
SET UNIQUE_CHECKS = 0;
/*!40000 ALTER TABLE `em_itsec_distributed_storage` ENABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 1;
SET UNIQUE_CHECKS = 1;
