<?php
/**
 * Listify child theme.
 */
function listify_child_styles() {
    wp_enqueue_style( 'listify-child', get_stylesheet_uri() );
}
add_action( 'wp_enqueue_scripts', 'listify_child_styles', 999 );

/** Place any new code below this line */

/**
 * Listify - Default Image for Listings
 */
function custom_default_listify_cover_image( $image, $args ) {
	global $post;
	
	if ( $image || $post->post_type != 'job_listing' ) {
		return $image;
	}
	
	$image = array( 'https://eldercarematters.com/wp-content/uploads/2017/10/back-marble-default-bg.jpg' );
	
	return $image;
}
add_filter( 'listify_cover_image', 'custom_default_listify_cover_image', 10, 2 );


/**
 * Listify - Remove info from system created URLs
 */
function listify_custom_remove_url_slugs() {
    add_filter( 'submit_job_form_prefix_post_name_with_location', '__return_false' );
}
add_action( 'after_setup_theme', 'listify_custom_remove_url_slugs' );


/**
 * Listify - Remove "Add Photos" Link
 */
function custom_listify_remove_action_links() { 
 global $listify_job_manager;
 
 remove_action( 'listify_single_job_listing_actions_start', array( $listify_job_manager->gallery, 'add_link' ) );
}
add_action( 'init', 'custom_listify_remove_action_links' );

/**
 * Plugin Name: Listify - Add messqge at checkout
 */
add_action( 'woocommerce_before_checkout_form', 'wnd_checkout_message', 10 );

function wnd_checkout_message( ) {
 echo '<div class="wnd-checkout-message"><h5>*Our billing cycle begins on the 1st of each month. When adding or claiming a listing, the initial payment will be prorated based on the remaining days in the month you sign up. Do not be concerned if the total amount below appears less than the package you selected.</h5>
</div>';
}

/**
 *  Add Contact Button to Single Listing
 */
/**
 *function custom_listify_single_job_listing_actions_after() {
 *    echo '<a href="#job_listing-author-apply" data-mfp-src=".job_application" class="popup-trigger button">' . __( 'Contact', 'listify' ) . '</a>';
 *}
 *add_filter( 'listify_single_job_listing_actions_after', 'custom_listify_single_job_listing_actions_after' );
 */


/* Change Excerpt length */
function custom_excerpt_length( $length ) {
return 30;
}
add_filter( ‘excerpt_length’, ‘custom_excerpt_length’, 999 );



/* ------- Line Break Shortcode --------*/
function line_break_shortcode() {
	return '<br />';
}
add_shortcode( 'br', 'line_break_shortcode' );


/* ------- Allow Subscribers to be set as customers --------*/
/* 
add_filter( 'wp_dropdown_users_args', 'add_subscribers_to_dropdown', 10, 2 );
function add_subscribers_to_dropdown( $query_args, $r ) {

    $query_args['who'] = '';
    return $query_args;
	

}*/

// enqueue script
function my_scripts_method() {
if ( !is_admin() ) {
 wp_enqueue_script('jquery-ui-accordion');
 wp_enqueue_script(
 'custom-accordion',
 get_stylesheet_directory_uri() . '/js/accordion.js',
 array('jquery')
 );
 }
}
add_action('wp_enqueue_scripts', 'my_scripts_method');

//add accordion stylesheet
wp_register_style('jquery-custom-style', get_stylesheet_directory_uri().'/css/jquery-ui-1.12.1.custom/jquery-ui.css', array(), '1', 'screen'); 
wp_enqueue_style('jquery-custom-style');

//add street to address
add_filter( 'listify_get_listing_jsonld', function( $markup, $listing ) {
	$location_data = $listing->get_location_data();

	if ( '' !== $location_data['address_1'] ) {
		$markup['address']['streetAddress'] = 
			( '' !== $location_data['street_number'] ? $location_data['street_number'] . ' ' : '' ) . 
			$location_data['address_1'] . 
			( '' !== $location_data['address_2'] ? ( ' ' . $location_data['address_2'] ) : '' );
	}

	return $markup;
}, 10, 2 );

//mod content
function hatom_mod_post_content ($content) {
  if ( in_the_loop() && !is_page() ) {
    $content = '<span class="entry-content">'.$content.'</span>';
  }
  return $content;
}
add_filter( 'the_content', 'hatom_mod_post_content');

//add hatom data
function add_mod_hatom_data($content) {
	$t = get_the_modified_time('F jS, Y');
	$author = get_the_author();
	$title = get_the_title();
	if(is_single()) {
		$content .= '<div style="margin-top:25px;font-style: italic;color: #999;" class="hatom-extra"><span class="entry-title">'.$title.'</span> was last modified: <span class="updated"> '.$t.'</span> by <span class="author vcard"><span class="fn">'.$author.'</span></span></div>';
	}
	return $content;
	}
add_filter('the_content', 'add_mod_hatom_data');