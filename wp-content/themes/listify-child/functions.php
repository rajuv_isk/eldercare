<?php
session_start();
/**
 * Listify child theme.
 */

/** Remove Order Again Button from CHeckout */
if ( ! function_exists( 'woocommerce_order_again_button' ) ) {
	function woocommerce_order_again_button( $order ) {
		return;
	}
}

function listify_child_styles() {
    wp_enqueue_style( 'listify-child', get_stylesheet_uri() );
    wp_register_style('jquery-custom-style', get_stylesheet_directory_uri().'/css/jquery-ui-1.12.1.custom/jquery-ui.css', array(), '1', 'screen');
    /*wp_enqueue_style('chosen-min-css','https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.min.css');*/
    wp_enqueue_script('chosen-min-js','https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.jquery.min.js');

}
add_action( 'wp_enqueue_scripts', 'listify_child_styles', 999 );

/** Place any new code below this line */

/**
 * Listify - Default Image for Listings
 */
function custom_default_listify_cover_image( $image, $args ) {
	global $post;

	if ( $image || $post->post_type != 'job_listing' ) {
		return $image;
	}

	$image = array( 'https://eldercarematters.com/wp-content/uploads/2017/10/back-marble-default-bg.jpg' );

	return $image;
}
add_filter( 'listify_cover_image', 'custom_default_listify_cover_image', 10, 2 );

/**
* Modify email display
*/
function new_email_button(){
	$email = listify_get_listing( $post )->get_email();

	if ( ! $email ) {
		return;
	}
	?>
	<style>
		.listing-email{
			display: none;
		}
	</style>
	<div class="listing-email" style="display: block">
		<a href="mailto:<?php echo esc_attr( antispambot( $email ) ); ?>?subject=Message from Elder Care Matters">Contact Via Email</a>
	</div>

	<?php
}

add_filter('listify_widget_job_listing_map_after', 'new_email_button');

/**
* Custom search text
*/
function new_filter_location_search($translated, $original, $domain){
  if($translated == "Location (Search by City or Zip)"){
    $translated = "Location (Search by City)";
  }else if($translated == "Order received"){
	$translated = "Account Created Successfully";
  }else if($translated == "Submit Job"){
  	$translated = "Submit Listing";
  }

  return $translated;
}
add_filter('gettext', 'new_filter_location_search', 10, 3);

function custom_category_text($fields){
	$fields['job']['job_category']['label'] = "Listing Category/Categories";
	return $fields;
}
add_filter('submit_job_form_fields', 'custom_category_text');

/**
 * Remove the preview step.
 * @param  array $steps
 * @return array
 */
function custom_submit_job_steps( $steps ) {
	unset( $steps['preview'] );
	return $steps;
}
add_filter( 'submit_job_steps', 'custom_submit_job_steps' );
/**
 * Change button text (won't work until v1.16.2)
 */

function change_preview_text() {
	return __( 'Submit Job' );
}
add_filter( 'submit_job_form_submit_button_text', 'change_preview_text' );

/**
 * Since we removed the preview step and it's handler, we need to manually publish jobs
 * @param  int $job_id
 */
function done_publish_job( $job_id ) {
	$job = get_post( $job_id );
	if ( in_array( $job->post_status, array( 'preview', 'expired' ) ) ) {
		// Reset expirey
		delete_post_meta( $job->ID, '_job_expires' );
		// Update job listing
		$update_job                  = array();
		$update_job['ID']            = $job->ID;
		$update_job['post_status']   = get_option( 'job_manager_submission_requires_approval' ) ? 'pending' : 'publish';
		$update_job['post_date']     = current_time( 'mysql' );
		$update_job['post_date_gmt'] = current_time( 'mysql', 1 );
		wp_update_post( $update_job );
	}
}
add_action( 'job_manager_job_submitted', 'done_publish_job' );

/**
 * Listify - Remove info from system created URLs
 */
function listify_custom_remove_url_slugs() {
    add_filter( 'submit_job_form_prefix_post_name_with_location', '__return_false' );
}
add_action( 'after_setup_theme', 'listify_custom_remove_url_slugs' );


/**
 * Listify - Remove "Add Photos" Link
 */
function custom_listify_remove_action_links() {
 global $listify_job_manager;

 remove_action( 'listify_single_job_listing_actions_start', array( $listify_job_manager->gallery, 'add_link' ) );
}
add_action( 'init', 'custom_listify_remove_action_links' );

/**
 * Plugin Name: Listify - Add messqge at checkout
 */
add_action( 'woocommerce_before_checkout_form', 'wnd_checkout_message', 10 );

function wnd_checkout_message( ) {
	//echo '<pre>';print_r(WC()->cart->get_cart());
 echo '<div class="wnd-checkout-message"><h5>*Our billing cycle begins on the 1st of each month. When adding or claiming a listing, the initial payment will be prorated based on the remaining days in the month you sign up. Do not be concerned if the total amount below appears less than the package you selected.</h5>
</div>';
}

/**
 *  Add Contact Button to Single Listing
 */
/**
 *function custom_listify_single_job_listing_actions_after() {
 *    echo '<a href="#job_listing-author-apply" data-mfp-src=".job_application" class="popup-trigger button">' . __( 'Contact', 'listify' ) . '</a>';
 *}
 *add_filter( 'listify_single_job_listing_actions_after', 'custom_listify_single_job_listing_actions_after' );
 */


/* Change Excerpt length */
/*function custom_excerpt_length( $length ) {
return 30;
}
add_filter( ‘excerpt_length’, ‘custom_excerpt_length’, 999 );*/
function custom_excerpt_length($limit) {
      $excerpt = explode(' ', get_the_excerpt(), $limit);

      if (count($excerpt) >= $limit) {
          array_pop($excerpt);
          $excerpt = implode(" ", $excerpt) . '...';
      } else {
          $excerpt = implode(" ", $excerpt);
      }

      $excerpt = preg_replace('`\[[^\]]*\]`', '', $excerpt);

      return $excerpt;
}



/* ------- Line Break Shortcode --------*/
function line_break_shortcode() {
	return '<br />';
}
add_shortcode( 'br', 'line_break_shortcode' );


/* ------- Allow Subscribers to be set as customers --------*/
/*
add_filter( 'wp_dropdown_users_args', 'add_subscribers_to_dropdown', 10, 2 );
function add_subscribers_to_dropdown( $query_args, $r ) {

    $query_args['who'] = '';
    return $query_args;


}*/

// enqueue script
function my_scripts_method() {
if ( !is_admin() ) {
 wp_enqueue_script('jquery-ui-accordion');
 wp_enqueue_script(
 'custom-accordion',
 get_stylesheet_directory_uri() . '/js/accordion.js',
 array('jquery')
 );
 }
}
add_action('wp_enqueue_scripts', 'my_scripts_method');

//add accordion stylesheet


//add street to address
add_filter( 'listify_get_listing_jsonld', function( $markup, $listing ) {
	$location_data = $listing->get_location_data();

	if ( '' !== $location_data['address_1'] ) {
		$markup['address']['streetAddress'] =
			( '' !== $location_data['street_number'] ? $location_data['street_number'] . ' ' : '' ) .
			$location_data['address_1'] .
			( '' !== $location_data['address_2'] ? ( ' ' . $location_data['address_2'] ) : '' );
	}

	return $markup;
}, 10, 2 );

//mod content
function hatom_mod_post_content ($content) {
  if ( in_the_loop() && !is_page() ) {
    $content = '<span class="entry-content">'.$content.'</span>';
  }
  return $content;
}
add_filter( 'the_content', 'hatom_mod_post_content');

//add hatom data
function add_mod_hatom_data($content) {
	$t = get_the_modified_time('F jS, Y');
	$author = get_the_author();
	$title = get_the_title();
	if(is_single()) {
		$content .= '<div style="margin-top:25px;font-style: italic;color: #999;" class="hatom-extra"><span class="entry-title">'.$title.'</span> was last modified: <span class="updated"> '.$t.'</span> by <span class="author vcard"><span class="fn">'.$author.'</span></span></div>';
	}
	return $content;
	}
add_filter('the_content', 'add_mod_hatom_data');

/*add_action('init','update_my_metadata');
function update_my_metadata(){
    $args = array(
        'post_type' => 'job_listing', // Only get the posts
        'post_status' => 'publish', // Only the posts that are published
        'posts_per_page'   => -1 // Get every post
    );
    $posts = get_posts($args);
    foreach ( $posts as $post ) {
        // Run a loop and update every meta data
        $package_id = get_post_meta($post->ID, '_package_id', true );
        $featured = get_post_meta($post->ID, '_featured', true );

        if ($featured == 1) {
        	$priority = 1;
        }else if ($package_id == 10481) {
        	$priority = 3;
        }else {
        	$priority = 2;
        }
        update_post_meta( $post->ID, '_job_sale', $priority );
    }
}*/


/*----------------------------------------------------*/


/*upgrade Reminder*/
add_action('wp_footer', 'iskmyscript');
function iskmyscript() {
	global $post;

if(is_singular('job_listing')) {
$package_id = get_post_meta( $post->ID, '_package_id', true );
if ($package_id == 10481) {
?>
<style type="text/css">
	.listing-rating{display: none;}
	.listify_widget_panel_listing_social_profiles{display: none;}
</style>
<script>
	jQuery(document).ready(function($) {
	jQuery(".job_listing-url a").attr("href", "javascript:void(0)");
    $('a[href="javascript:void(0)"]').click(function(e) {
        e.preventDefault();
    });
});

</script>
<?php
 }
 }

 if(is_page(82)) {
 	if ($_POST['step'] == 1) {
 		$job_id = $_POST['job_id'];
 		if ($job_id == 0) {
 			$package_id = $_POST['wcpl_jmfe_product_id'];
 		}else{
 		$job_id = $_POST['job_id'];
        $package_id = get_post_meta( $job_id, '_wcpl_jmfe_product_id', true );
 		}
 	}
if ($package_id == 10481) {
?>
<style type="text/css">
	.listing-cover .content-single-job_listing-hero-actions{display: none;}
	.listing-cover .listing-rating{display: none;}
	.listify_widget_panel_listing_social_profiles{display: none;}
</style>
<script>
	jQuery(document).ready(function($) {
	jQuery(".job_listing-url a").attr("href", "javascript:void(0)");
    $('a[href="javascript:void(0)"]').click(function(e) {
        e.preventDefault();
    });
});

</script>
<?php
 }
 }
  ?>
 <script>
 	jQuery(document).ready(function() {
 		if (jQuery('.job-package span.subscription-details').length) {
 			jQuery('span.subscription-details').each(function() {
 				var text = jQuery(this).text();
 				jQuery(this).text(text.replace('on the 1st of each month', '/month'));
 			});
 		}
 	});
 </script>
<?php
}


function eld_restriction(){
	global $current_user,$wpdb;
	$user_id = $current_user->ID;
	$table_name = $wpdb->prefix . "wcpl_user_packages";

	if(is_user_logged_in() && $_POST['job_manager_form'] == 'submit-job' && $_POST['step'] == 0){
		$job_package = $_POST['job_package'];
		$jobPackage = explode("-",$job_package);
		$job_package_ID = $jobPackage[1];
		$package = $wpdb->get_row( "SELECT * FROM $table_name WHERE `id` = $job_package_ID");
		$product_id = $package->product_id;
        if ($product_id == 10481) {
		?>
		<script>
			jQuery(function($){
				if (jQuery(window).width() < 960) {


				var selector = jQuery('#job_category');
				selector.removeAttr("multiple");
				   /*var last_valid_selection = null;
				     selector.change(function(event) {
				       if (jQuery(this).val().length > 1) {
				         jQuery(this).val(last_valid_selection);
				       } else {
				         last_valid_selection =jQuery(this).val();
				       }
				     });*/
				 }
				jQuery( '#job_category' ).chosen({
					max_selected_options: 1,
					search_contains: true
				});
                /*if (jQuery(window).width() < 960) {
				var selector = jQuery('#job_category');
				   var last_valid_selection = null;
				     selector.change(function(event) {
				       if (jQuery(this).val().length > 1) {
				         jQuery(this).val(last_valid_selection);
				       } else {
				         last_valid_selection = jQuery(this).val();
				       }
				     });
				 }*/

			});


			jQuery(document).ready(function(){

				jQuery(function ($) {
					var max = 3;
					var checkboxes = $('input[type="checkbox"]');
					checkboxes.change(function () {
						var current = checkboxes.filter(':checked').length;
						checkboxes.filter(':not(:checked)').prop('disabled', current >= max);
					}).change();
				});

			});
		</script>
		<?php
	   }
	}

	if($_POST['edit_job'] == 'Edit listing' && $_POST['step'] = 2){
		$job_id = $_POST['job_id'];
		$package_id = get_post_meta( $job_id, '_wcpl_jmfe_product_id', true );
        if ($package_id == 10481) {
		?>
		<script>
			jQuery(function($){
				$( '#job_category' ).chosen({
					max_selected_options: 1,
					search_contains: true
				});
			});

			jQuery(document).ready(function(){
				jQuery(function ($) {
					var max = 3;
					var checkboxes = $('input[type="checkbox"]');
					checkboxes.change(function () {
						var current = checkboxes.filter(':checked').length;
						checkboxes.filter(':not(:checked)').prop('disabled', current >= max);
					}).change();
				});

			});
		</script>
		<?php
	   }
	}

	if (is_page(86) && $_GET['action'] == 'edit') {
		$job_id = $_GET['job_id'];
		$package_id = get_post_meta($job_id, '_package_id', true );
		if ($package_id == 10481) { ?>
			<style type="text/css">
			.fieldset-company_video{display: none;}
		</style>
		<script>
			jQuery(function($){
				if (jQuery(window).width() < 960) {


				var selector = jQuery('#job_category');
				selector.removeAttr("multiple");
				   /*var last_valid_selection = null;
				     selector.change(function(event) {
				       if (jQuery(this).val().length > 1) {
				         jQuery(this).val(last_valid_selection);
				       } else {
				         last_valid_selection =jQuery(this).val();
				       }
				     });*/
				 }
				jQuery('#job_category').chosen({
					max_selected_options: 1,
					search_contains: true
				});

			});



			jQuery(document).ready(function(){



				jQuery(function ($) {
					var max = 3;
					var checkboxes = jQuery('input[type="checkbox"]');
					checkboxes.change(function () {
						var current = checkboxes.filter(':checked').length;
						checkboxes.filter(':not(:checked)').prop('disabled', current >= max);
					}).change();
				});

			});
		</script>

		<?php }}

		if(!empty($_GET['upgrade']) && is_page(82)) {
				$pID = $_GET['upgrade'];
				?>
				<script>
					jQuery(window).load(function(){
						var pid = '<?php echo $pID ?>';
						jQuery(".job-package-purchase .button").each(function() {
							var pval = jQuery(this).val();
							if (pval == 10481) {
								jQuery(this).prop('disabled', true);
						    	//jQuery(this).parents('ul').addClass('job-packages--count-2 upgrade').removeClass('job-packages--count-3');
						    	//jQuery(this).parents('li').hide();
						    	//jQuery(this).text('Get Started Now →');
						    	jQuery(this).hide();
						    }else{
						    	jQuery(this).text('Upgrade Now →');
						    }
						});
					});

					/*if (pid != '') {
						jQuery('button.button').click(function(){
							jQuery.ajax({
								type: 'POST',
								url : '<?php //echo admin_url( 'admin-ajax.php' ); ?>',
								data:
								{
									action: 'send_upgradeid',
									subid: pid,
								},
								success: function(data){

								}
							});
						});
					}*/

				</script>

			<?php

        $_SESSION['wdm_user_custom_data'] = $pID;
		}


}
add_action('wp_footer', 'eld_restriction');

/*Make comments open by default for new job listings*/
add_action( 'job_manager_job_submitted', 'isk_save_post');
function isk_save_post($job_id) {
	global $wpdb;
	$package_id = get_post_meta($job_id, '_package_id', true );
	if ($package_id == 91) {
		$priority = 1;
	}
	if ($package_id == 92) {
		$priority = 2;
	}
	if ($package_id == 10481){
		$priority = 3;
	}

	update_post_meta( $job_id, '_job_sale', $priority );
	$job_sale = get_post_meta( $job_id, '_job_sale', true);
	if ($job_sale == 3){
		wp_update_post(array('ID' => $job_id, 'comment_status' => 'closed'));
		$wpdb->update( $wpdb->posts, array( 'comment_status' => 'closed' ), array( 'ID' => $job_id ) );
	}

}

/*
 * Save/Update Listing when Save/Update from Frontend
 */
add_action( 'job_manager_update_job_data', 'isk_update_job_fields', 100, 2 );

function isk_update_job_fields( $job_id, $values ){
	global $wpdb;
	$job_sale = get_post_meta( $job_id, '_job_sale', true);
	if ($job_sale == 3){
		wp_update_post(array('ID' => $job_id, 'comment_status' => 'closed'));
		$wpdb->update( $wpdb->posts, array( 'comment_status' => 'closed' ), array( 'ID' => $job_id ) );
	}

}

/*Order items randomly*/

/*add_action( 'init', function() {
	add_filter( 'job_manager_get_listings', function( $query_args, $args ) {
		$query_args['disable_cache']  = time();
		return $query_args;
	}, 99, 2 );
	add_filter( 'get_job_listings_query_args', function( $query_args, $args = array() ) {
		parse_str( $_REQUEST['form_data'], $request );
        if ( '' === $request['search_sort'] ) {
        $query_args[ 'orderby' ] = 'meta_value_num';
        $query_args[ 'order' ] = 'ASC';
        $query_args[ 'meta_key' ] = '_job_sale';
        }
		#$query_args['orderby'] = 'rand';
		return $query_args;
	}, 99, 2 );
} );*/

add_action( 'init', function() {
	add_filter( 'job_manager_get_listings', function( $query_args, $args ) {
         $query_args['disable_cache']  = time();
		return $query_args;
	}, 99, 2 );
	add_filter( 'get_job_listings_query_args', function( $query_args, $args = array() ) {
		//parse_str( $_REQUEST['form_data'], $request );
        //if ( '' === $request['search_sort'] ) {
        $query_args[ 'orderby' ] = 'meta_value_num';
        $query_args[ 'order' ] = 'ASC';
        $query_args[ 'meta_key' ] = '_job_sale';
        //}
		//$query_args['orderby'] = 'rand';
		//echo '<pre>';print_r($query_args);
		return $query_args;
	}, 99, 2 );
} );

/*upgrade reminder*/
add_action( 'woocommerce_before_my_account', 'isk_get_logincustomer_orders' );
function isk_get_logincustomer_orders() {
	$users_subscriptions = wcs_get_users_subscriptions($user_id);
	$product_ids = array();
	$customer = wp_get_current_user();
	$bought = false;
	$prod_arr = array(10481);
	foreach ($users_subscriptions as $subscription){

		if ($subscription->has_status(array('active'))) {

			if ( sizeof( $subscription_items = $subscription->get_items() ) > 0 ) {

				foreach ( $subscription_items as $item_id => $item ) {
					//echo '<pre>';print_r($item);

					//$product_ids[] = array("subs_id"=>$item['order_id'], "plan_id"=>$item['product_id'] );
					$has_sub = wcs_user_has_subscription(get_current_user_id(), 91, 'active' );

					$has_sub2 = wcs_user_has_subscription(get_current_user_id(), 92, 'active' );

					$has_sub3 = wcs_user_has_subscription(get_current_user_id(), 10481, 'active' );

					if ($has_sub == true && $has_sub2 == true && $has_sub3 == true) {
						$bought = false;
					}elseif ($has_sub == true && $has_sub2 == false && $has_sub3 == true) {
						$bought = true;
					}elseif ($has_sub == false && $has_sub2 == true && $has_sub3 == true) {
						$bought = true;
					}elseif ($has_sub == false && $has_sub2 == false && $has_sub3 == true) {
						$bought = true;
					}
					if ($item['product_id'] == 10481) {
						if ($bought) {
							$planpageurl = get_the_permalink(82).'?upgrade='.$item['order_id'];

							$notice_text = sprintf( 'Hey %1$s &#x1f600; Ready to have your upgrade Free Listing?', $customer->display_name);


							echo $noticeurl = '<div class="woocommerce-info reminderdiv">'.$notice_text.'<a class="fragment1" href="'.$planpageurl.'"><span class="shoppagebtn">&nbsp Upgrade free Listing</span></a><a href="javascript:void(0)" id="closediv"><span class="shoppagebtn">  &nbsp Close this reminder</span></div>';
						}
					}
				}
			}
		}
	}
	?>
	<script>
		jQuery(document).ready(function() {
			jQuery('#closediv').click(function(){
				jQuery('.reminderdiv').hide();
			});
		});
	</script>
	<?php
}

/*add_action('wp_ajax_send_upgradeid', 'send_upgradeid');
add_action('wp_ajax_nopriv_send_upgradeid', 'send_upgradeid');
function send_upgradeid(){
      $user_custom_data_values =  $_POST['subid'];
      session_start();
      $_SESSION['wdm_user_custom_data'] = $user_custom_data_values;
      die();
}*/

add_filter('woocommerce_add_cart_item_data','wdm_add_item_data',1,2);
if(!function_exists('wdm_add_item_data')){
    function wdm_add_item_data($cart_item_data,$product_id)  {
        /*Here, We are adding item in WooCommerce session with, wdm_user_custom_data_value name*/
        global $woocommerce;
        session_start();
        if (isset($_SESSION['wdm_user_custom_data'])) {
            $option = $_SESSION['wdm_user_custom_data'];
            $new_value = array('wdm_user_custom_data_value' => $option);
        }
        if(empty($option))
            return $cart_item_data;
        else
        {
            if(empty($cart_item_data))
                return $new_value;
            else
                return array_merge($cart_item_data,$new_value);
        }
        unset($_SESSION['wdm_user_custom_data']);
        //Unset our custom session variable, as it is no longer needed.
    }
}

add_filter('woocommerce_get_cart_item_from_session', 'wdm_get_cart_items_from_session', 1, 3 );
if(!function_exists('wdm_get_cart_items_from_session'))
{
    function wdm_get_cart_items_from_session($item,$values,$key)
    {
        if (array_key_exists( 'wdm_user_custom_data_value', $values ) )
        {
        $item['wdm_user_custom_data_value'] = $values['wdm_user_custom_data_value'];
        }
        return $item;
    }
}

add_action('woocommerce_add_order_item_meta','wdm_add_values_to_order_item_meta',1,2);
if(!function_exists('wdm_add_values_to_order_item_meta'))
{
  function wdm_add_values_to_order_item_meta($item_id, $values)
  {
        global $woocommerce,$wpdb;
        $user_custom_values = $values['wdm_user_custom_data_value'];
        if(!empty($user_custom_values))
        {
            wc_add_order_item_meta($item_id,'wdm_user_custom_data',$user_custom_values);
        }
  }
}

add_action('woocommerce_before_cart_item_quantity_zero','wdm_remove_user_custom_data_options_from_cart',1,1);
if(!function_exists('wdm_remove_user_custom_data_options_from_cart'))
{
    function wdm_remove_user_custom_data_options_from_cart($cart_item_key)
    {
        global $woocommerce;
        // Get cart
        $cart = $woocommerce->cart->get_cart();
        // For each item in cart, if item is upsell of deleted product, delete it
        foreach( $cart as $key => $values)
        {
        if ( $values['wdm_user_custom_data_value'] == $cart_item_key )
            unset( $woocommerce->cart->cart_contents[ $key ] );
        }
    }
}

add_action( 'woocommerce_thankyou', 'isk_thankyou_subscription_action', 50, 1 );
function isk_thankyou_subscription_action( $order_id ){
	global $wpdb;
	if( ! $order_id ) return;
	$order = wc_get_order( $order_id );
	foreach ( $order->get_items() as $item_id => $item ) {
		//echo '<pre>';print_r($item);
		$packageID = $item['product_id'];
		$old_subscription_id = wc_get_order_item_meta( $item_id, 'wdm_user_custom_data', true );
		if ($old_subscription_id) {

			$user_package      = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM {$wpdb->prefix}wcpl_user_packages WHERE order_id IN ( %d)", $old_subscription_id) );

			$userlisting      = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM {$wpdb->prefix}postmeta WHERE meta_value IN ( %d)", $user_package->id) );
			$jobID = $userlisting->post_id;
			if ($jobID == '') {
				$count = 0;
			}else{
				$count = 1;
			}
			if ($packageID == 91) {
				$featured = 1;
			}else{
				$featured = 0;
			}

			if ($packageID == 91) {
				$priority = 1;
			}else if ($packageID == 92) {
				$priority = 2;
			}else if ($packageID == 10481){
				$priority = 3;
			}

			$subscriptions = wcs_get_subscriptions_for_order( $item['order_id'] );
			foreach( $subscriptions as $subscription_id => $subscription ){
				$subsid = $subscription->id;
				$wpdb->update(
					"{$wpdb->prefix}wcpl_user_packages",
					array(
						'package_count'  => $count,
					),
					array(
						'order_id'   => $subsid,
					)
				);
			}

			$newuser_package      = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM {$wpdb->prefix}wcpl_user_packages WHERE order_id IN ( %d)", $subsid) );
			$newuser_package_id = $newuser_package->id;

			update_post_meta($jobID, '_user_package_id',$newuser_package_id);
			update_post_meta($jobID, '_package_id',$packageID);
			update_post_meta($jobID, '_wcpl_jmfe_product_id',$packageID);
			update_post_meta($jobID, '_job_sale',$priority);
			update_post_meta($jobID, '_featured',$featured);
			wp_update_post(array('ID' => $jobID, 'comment_status' => 'open'));
			$oldsubscription = new WC_Subscription( $old_subscription_id );
			$old_orderID = method_exists( $oldsubscription, 'get_parent_id' ) ? $oldsubscription->get_parent_id() : $oldsubscription->order->id;
			wp_delete_post($old_orderID,true);

           $current_user = wp_get_current_user();
		   $admin_email = get_option( 'admin_email' );
		   $user_email = $current_user->user_email;
	       $email_subject = "Switch Subscription". ': #' . $newuser_package->order_id;
	       $mailer = WC()->mailer();
	       $content = isk_switch_subscription_email_html( $order, $subject, $mailer );
	       $content2 = isk_switch_subscription_admin_email_html( $order, $subject, $mailer );
	       $headers = "Content-Type: text/html\r\n";
	       $mailer->send( $user_email, $email_subject, $content, $headers );
	       $mailer->send( $admin_email, $email_subject, $content2, $headers );
	       echo "hello";
		}
		unset($_SESSION['wdm_user_custom_data']);
	}
}

/*add_filter( 'woocommerce_add_cart_item_data', 'woo_custom_add_to_cart' );
function woo_custom_add_to_cart( $cart_item_data ) {
    global $woocommerce;
    $woocommerce->cart->empty_cart();
    return $cart_item_data;
}*/

/*Custom email when switch subscription */
function isk_switch_subscription_email_html( $order, $heading = false, $mailer ) {
    $template = 'emails/customer-completed-switch-order.php';
    return wc_get_template_html( $template, array(
        'order'         => $order,
        'email_heading' => $heading,
        'sent_to_admin' => false,
        'plain_text'    => false,
        'email'         => $mailer
    ) );
}

/*Admin email when switch subscription */
function isk_switch_subscription_admin_email_html( $order, $heading = false, $mailer ) {
    $template = 'emails/admin-new-switch-order.php';
    return wc_get_template_html( $template, array(
        'order'         => $order,
        'email_heading' => $heading,
        'sent_to_admin' => false,
        'plain_text'    => false,
        'email'         => $mailer
    ) );
}


/*Hide container meta in emails*/
function hide_subs_item_meta_in_emails( $meta ) {
        $criteria = array(  'key' => 'wdm_user_custom_data' );
        $meta = wp_list_filter( $meta, $criteria, 'NOT' );
    return $meta;
}
add_filter( 'woocommerce_order_item_get_formatted_meta_data', 'hide_subs_item_meta_in_emails' );

/*Changes the redirect URL for the Return To Shop button in the cart*/
function sk_empty_cart_redirect_url() {
	$add_your_listing = get_the_permalink(76);
	return $add_your_listing;
}
add_filter( 'woocommerce_return_to_shop_redirect', 'sk_empty_cart_redirect_url' );

function add_free_listing_body_class( $classes ) {    
global $post;
$freelisting = get_post_meta($post->ID, '_job_sale', true );
    if ( isset ( $post->ID ) && ($freelisting == 3)) {
          $classes[] = 'has-free-listing';
 }
          return $classes;
}
add_filter( 'body_class', 'add_free_listing_body_class' );

/*Phone format*/
add_action('wp_footer', 'isk_phone_format_script');
function isk_phone_format_script() {
?>
 <script>
 	jQuery(document).ready(function() {
 		jQuery('input[type=tel],.field #phone').keydown(function (e) {
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                return false;
            }
            var curchr = this.value.length;
            var curval = jQuery(this).val();
            if (curchr == 3 && e.which != 8 && e.which != 0) {
                jQuery(this).val(curval + "-");
            } else if (curchr == 7 && e.which != 8 && e.which != 0) {
                jQuery(this).val(curval + "-");
            }
            jQuery(this).attr('maxlength', '12');
        });

        /*hide 0 reviews from listing detail page*/
        jQuery(".single-job_listing .listing-rating .listing-rating-count a:contains('0 Reviews')").hide();
 	});
 	jQuery(window).load(function() {
        /*hide 0 reviews from listing detail page*/
        jQuery(".single-job_listing .listing-rating .listing-rating-count a:contains('0 Reviews')").hide();
        jQuery(".single-job_listing .listing-rating .listing-rating-count a").css("visibility", "visible");
         jQuery(".single-job_listing .listing-rating span.listing-rating-count:contains('0 Reviews')").hide();
        jQuery(".single-job_listing .listing-rating .listing-rating-count").css("visibility", "visible");
 	});
 </script>
<?php
}

