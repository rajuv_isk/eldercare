<?php
/**
 * Customer new account email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/customer-new-account.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates/Emails
 * @version 3.5.2
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

?>

<?php do_action( 'woocommerce_email_header', $email_heading, $email ); ?>

<?php /* translators: %s Customer username */ ?>
<p><?php printf( esc_html__( 'Hi %s,', 'woocommerce' ), esc_html( $user_login ) ); ?></p>
<?php /* translators: %1$s: Site title, %2$s: Username, %3$s: My account link */ ?>
<p><?php printf( __( 'Welcome and congratulations on your new listing as an Elder Care Provider on %1$s. Your username is %2$s. You can access your account area to view orders, change your password, and more at: %3$s', 'woocommerce' ), esc_html( $blogname ), '<strong>' . esc_html( $user_login ) . '</strong>', make_clickable( esc_url( wc_get_page_permalink( 'myaccount' ) ) ) ); ?></p><?php // phpcs:ignore WordPress.XSS.EscapeOutput.OutputNotEscaped ?>

<?php if ( 'yes' === get_option( 'woocommerce_registration_generate_password' ) && $password_generated ) : ?>
	<?php /* translators: %s Auto generated password */ ?>
	<p><?php printf( esc_html__( 'Your password has been automatically generated: %s', 'woocommerce' ), '<strong>' . esc_html( $user_pass ) . '</strong>' ); ?></p>
<?php endif; ?>

<?php echo '<p>Below you will find the marketing material that we ask for you to add to your company website in order to let families across America know that you are listed on <a href="https://www.eldercarematters.com">ElderCareMatters.com</a>.</p>
<h3>Member Badge - For Use on the Web</h3>
<p>To add our badge to your website, please copy the code below and paste within your site. If your IT staff has any issues, please let us know! We can help.</p>
<p style="padding-left: 30px;"><a href="https://eldercarematters.com"><img src="https://eldercarematters.com/wp-content/uploads/2019/03/membership-badge-eldercare-matters-edited.jpg" alt="ElderCare Matters - National Elder Care Directory" width="325" height="267" /></a></p>
<p></p>
<h3>Member Badge - For Use in Print</h3>
<p><a href="https://eldercarematters.com/wp-content/uploads/2019/03/membership-badge-eldercare-matters-edited.jpg"><img class="alignleft wp-image-10633 size-full" style="max-width: 50%;" src="https://eldercarematters.com/wp-content/uploads/2019/03/membership-badge-eldercare-matters-edited.jpg" alt="" width="600" height="494"></a></p><p>To Download our web logo, you can right-click the logo image, and then "Save image as..." to save the image to your computer or device.</p>' ?>

<p><?php esc_html_e( 'Thank you for your listing on ElderCareMatters.com.', 'woocommerce' ); ?></p>

<?php
do_action( 'woocommerce_email_footer', $email );