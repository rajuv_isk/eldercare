<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package Listify
 */
?>

	</div><!-- #content -->

</div><!-- #page -->

<div class="footer-wrapper">

	<?php if ( ! listify_is_job_manager_archive() ) : ?>

		<?php get_template_part( 'content', 'aso' ); ?>

		<?php if ( is_active_sidebar( 'widget-area-footer-1' ) || is_active_sidebar( 'widget-area-footer-2' ) || is_active_sidebar( 'widget-area-footer-3' ) ) : ?>

			<footer class="site-footer-widgets">
				<div class="container">
					<div class="row">

						<div class="footer-widget-column col-12 col-sm-12 col-lg-5">
							<?php dynamic_sidebar( 'widget-area-footer-1' ); ?>
						</div>

						<div class="footer-widget-column col-12 col-sm-6 col-lg-4">
							<?php dynamic_sidebar( 'widget-area-footer-2' ); ?>
						</div>

						<div class="footer-widget-column col-12 col-sm-6 col-lg-2  offset-lg-1">
							<?php dynamic_sidebar( 'widget-area-footer-3' ); ?>
						</div>

					</div>
				</div>
			</footer>

		<?php endif; ?>

	<?php endif; ?>

	<footer id="colophon" class="site-footer">
		<div class="container">

			<div class="site-info">
				<?php echo listify_partial_copyright_text(); ?>
			</div><!-- .site-info -->

			<div class="site-social">
				<?php
				wp_nav_menu(
					array(
						'theme_location' => 'social',
						'menu_class'     => 'nav-menu-social',
						'fallback_cb'    => '',
						'depth'          => 1,
					)
				);
				?>
			</div>

		</div>
	</footer><!-- #colophon -->

</div>

<!-- code written on 19aug19 -->
<?php if(get_the_ID()==82 && !is_user_logged_in()){ ?>
	<script type="text/javascript">
		jQuery(window).load(function(){
			console.log('removing');
			jQuery('select#job_category').removeAttr('multiple');
		});
		
	</script>
<?php } ?>

<?php if($_GET['key']!=''){ ?>
	<style>
		.loginshow{display: flex;}
		.loginshow a.button{display: block;}
	</style>
<?php }else{ ?>
	<style>
		.loginshow{display: none;}
	</style>
<?php } ?>

<?php if(is_home()|| is_front_page()){ ?>
	<script type="text/javascript">
		jQuery(window).load(function(){
			console.log('eldercaredummy.png');
			jQuery('.blog-archive article').css('background','url("<?php echo get_stylesheet_directory_uri();?>/images/eldercaredummy.png")');
		});
		
	</script>
<?php } ?>

<!-- <script type="text/javascript">
	jQuery(window).ready(function(){
		jQuery('.listingbtn').append(jQuery('.primary.nav-menu li:last-child').html());
	});
</script> -->

<div id="ajax-response"></div>

<?php wp_footer(); ?>

</body>
</html>