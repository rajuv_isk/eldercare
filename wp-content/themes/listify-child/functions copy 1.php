<?php
/**
 * Listify child theme.
 */
function listify_child_styles() {
    wp_enqueue_style( 'listify-child', get_stylesheet_uri() );
}
add_action( 'wp_enqueue_scripts', 'listify_child_styles', 999 );

/** Place any new code below this line */

/**
 * Listify - Default Image for Listings
 */
function custom_default_listify_cover_image( $image, $args ) {
	global $post;
	
	if ( $image || $post->post_type != 'job_listing' ) {
		return $image;
	}
	
	$image = array( 'https://eldercarematters.com/wp-content/uploads/2017/10/back-marble-default-bg.jpg' );
	
	return $image;
}
add_filter( 'listify_cover_image', 'custom_default_listify_cover_image', 10, 2 );


/**
 * Listify - Remove info from system created URLs
 */
function listify_custom_remove_url_slugs() {
    add_filter( 'submit_job_form_prefix_post_name_with_location', '__return_false' );
}
add_action( 'after_setup_theme', 'listify_custom_remove_url_slugs' );


/**
 * Listify - Fix homepage location filter to show current city
 */
function listify_custom_autolocation() {
	if ( ! ( is_front_page() || listify_is_job_manager_archive() ) ) {
		return;
	}

	if ( isset( $_GET['search_location'] ) ) {
		return;
	}
?>
	<script>
		jQuery(document).ready(function() {
			var locate = jQuery( '.locate-me' );
			locate.trigger( 'click' );
		});
	</script>
<?php
}
add_action( 'wp_footer', 'listify_custom_autolocation', 9999 );


/**
 * Plugin Name: Listify - Remove "Add Photos" Link
 */
function custom_listify_remove_action_links() { 
 global $listify_job_manager;
 
 remove_action( 'listify_single_job_listing_actions_start', array( $listify_job_manager->gallery, 'add_link' ) );
}
add_action( 'init', 'custom_listify_remove_action_links' );

/**
 * Plugin Name: Listify - Remove "Add Photos" Link
 */
add_action( 'woocommerce_before_checkout_form', 'wnd_checkout_message', 10 );

function wnd_checkout_message( ) {
 echo '<div class="wnd-checkout-message"><h5>*Our billing cycle begins on the 1st of each month. When adding or claiming a listing, the initial payment will be prorated based on the remaining days in the month you sign up. Do not be concerned if the total amount below appears less than the package you selected.</h5>
</div>';
}

/**
 *  Add Contact Button to Single Listing
 */
/**
 *function custom_listify_single_job_listing_actions_after() {
 *    echo '<a href="#job_listing-author-apply" data-mfp-src=".job_application" class="popup-trigger button">' . __( 'Contact', 'listify' ) . '</a>';
 *}
 *add_filter( 'listify_single_job_listing_actions_after', 'custom_listify_single_job_listing_actions_after' );
 */


/* Change Excerpt length */
function custom_excerpt_length( $length ) {
return 30;
}
add_filter( ‘excerpt_length’, ‘custom_excerpt_length’, 999 );



/* ------- Line Break Shortcode --------*/
function line_break_shortcode() {
	return '<br />';
}
add_shortcode( 'br', 'line_break_shortcode' );