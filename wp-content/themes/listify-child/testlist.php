<?php
/* Template Name: The List Comp */
get_header();
$id = get_the_ID();
global $wpdb;
/*$user_package      = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM {$wpdb->prefix}wcpl_user_packages WHERE order_id IN ( %d)", $subscription->id) );*/
/*$subscription = new WC_Subscription( $subscription_id );
$order_id = method_exists( $subscription, 'get_parent_id' ) ? $subscription->get_parent_id() : $subscription->order->id;*/
/*$order = wc_get_order( $order_id );
    foreach ( $order->get_items() as $item_id => $item ) {

        }*/
       /* $wpdb->update(
                    "{$wpdb->prefix}wcpl_user_packages",
                    array(
                        'package_count'  => 1,
                    ),
                    array(
                        'order_id'   => $subsid,
                    )
                );*/
?>
<style type="text/css">
	.job_listing.type-job_listing.card-style--default.style-grid.listing-card.job_position_featured.listing-featured--badge {

    padding: 10px 10px;
    border: 2px solid gray;
    margin-right: 16px;
    width: 29%;
    float: left;
    margin: 0 10px 10px 10px;
    min-height: 100px;

}
span.featured {
    color: #08429c;
    background: none;

}
span.job_sale {

    background: blue;
    color: #fff;
    padding: 1px;

}
span {

    background: red;
    color: #fff;
    padding: 1px;

}
</style>
<div id="primary" class="container">
		<div class="row content-area">
<main id="main" class="site-main  col-12 job_filters--boxless" role="main">
<?php /*$args = array(
      'post_type' => 'job_listing',
      'posts_per_page' => -1,
      'orderby' => array('meta_value_num', 'date'),
      'order'   => array('ASC','DESC'),
      'meta_key' => '_job_sale'


    );*/
    $args = array ( 
	'post_type' => 'job_listing',
	'posts_per_page' => -1,
	'post_status' => 'publish',
	'meta_query' => array( 
		'relation' => 'AND',	
        'feature_clause' => array(
            'key' => '_job_sale',
            'value' => array(1,2,3),
            'type' => 'NUMERIC',
            'compare' => 'IN',
        ),	
        'total_clause'    => array(
            'key'     => '_wpjms_visits_total',
            'type' => 'NUMERIC',
            'compare' => 'EXISTS',
        ),        
    ),
	'orderby' => array(
        '_job_sale' => 'ASC',
		'total_clause' => 'DESC',               
	),
	'fields' => 'ids',
    'post_date'  => '', 
);
    $query = new WP_Query( $args ); 
    //echo '<pre>';print_r($query);

echo '<ul class="job_listings listing-cards-anchor--active" data-card-columns="3">';
    if( $query->have_posts() ): 
      while( $query->have_posts()): $query->the_post(); 
        $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
        $job_id = get_the_ID();
        $featured = get_post_meta($job_id, '_featured', true );
        if ($featured) {
        	$featured = 'featured';
        	$bg = 'gray';
        }else{
        	$featured = '';
        	$bg = '';
        }
        $package_id = get_post_meta($job_id, '_package_id', true );
        $views = get_post_meta($job_id, '_wpjms_visits_total', true );
        $job_sale = get_post_meta($job_id, '_job_sale', true );?>

        <li style="background: <?php echo $bg ;?>" class="job_listing type-job_listing card-style--default style-grid listing-card type-job_listing style-grid job_position_featured listing-featured--badge"><?php echo $views.' '.get_the_title().' <strong style="color:green">'.$job_id;  ?></strong>
        	<?php if ($featured != '') { ?>
        		<span class="<?php echo $featured;  ?>"><?php echo $featured;  ?> </span>
        	<?php }?>
        </br>
        	<span class="job_sale"><?php echo $job_sale;  ?></span>

        	<?php if ($package_id) { ?>
        		<span><?php echo $package_id;  ?></span>
        	<?php }?>
        	
        	  </li>
        
  <?php endwhile;  endif; wp_reset_query();
echo '</ul>';
?>
</main>
</div>
</div>

<?php
//session_start();

//echo "hh".$_SESSION['wdm_user_custom_data'];

//unset($_SESSION['wdm_user_custom_data']);
get_footer(); ?>