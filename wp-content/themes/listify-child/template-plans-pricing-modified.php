<?php
/**
 * Template Name: Page: Plans and Pricing Modified
 *
 * @package Listify
 */

get_header(); ?>

	<?php
	while ( have_posts() ) :
		the_post();
?>

		<div 
		<?php
		echo apply_filters(
			'listify_cover', 'page-cover', array(
				'size' => 'full',
			)
		);
		?>
		>
			<h1 class="page-title cover-wrapper"><?php the_title(); ?></h1>
		</div>

		<?php do_action( 'listify_page_before' ); ?>

		<div id="primary" class="container">
			<div class="content-area">

				<main id="main" class="site-main" role="main">

					<?php get_template_part( 'content', 'page' ); ?>

					<?php
					if ( listify_has_integration( 'wp-job-manager-wc-paid-listings' ) || listify_has_integration( 'wp-job-manager-listing-payments' ) ) {
						$defaults = array(
							'before_widget' => '<aside class="listify_widget_wcpl">',
							'after_widget'  => '</aside>',
							'before_title'  => '<div class="home-widget-section-title"><h3 class="home-widget-title">',
							'after_title'   => '%s</h3></div>',
							'widget_id'     => '',
						);

						the_widget(
							'Listify_Widget_WCPL_Pricing_Table',
							array(
								'title'       => '',
								'description' => '',
								'stacked'     => is_page_template( 'page-templates/template-plans-pricing-stacked.php' ),
							),
							$defaults
						);
					}
					?>
				<div class="content-box content-box-wrapper" style="margin-top:45px;">	
				<div class="content-box-inner">
					<div class="entry-content">Currently, our online eldercare directory is available throughout the United States. If you are located outside the United States and want to stay informed when eldercarematters.com is available in your region, please join our mailing list!</div>
				</div>
				</div>
						

				</main>

			</div>
		</div>

	<?php endwhile; ?>

<?php get_footer(); ?>